import { BasePage } from "./base-page";
import { PageURL } from "@test-data/page-url";
import { ElementWrapper } from "@utilities/protractor/element-wrapper";
import { by } from 'protractor'
import { Logger, LogTag } from "@utilities/general/logger";
import { PageTitle } from "@test-data/page-title";
import { exceptions } from "@utilities/general/exceptions";

let xPath = {
    PAGE_TITLE_SPAN: '//div[@class="app-container"]//div[@class="container-fluid bg-tmdblack shadow-sm page-header"]/span',
    USER_ADDRESS: '//div[@id="collapsibleNavbar"]//li[@class="nav-item dropdown nav-item-highlight"]/a[@id="navbardrop"]',
    LOGOUT_LINK: '//div[@id="collapsibleNavbar"]//li[@class="nav-item dropdown nav-item-highlight show"]/div[@class="dropdown-menu dropdown-menu-right show"]/a[last()]'
}

export class TestManagementPage extends BasePage {

    url: string = PageURL.TEST_MANAGEMENT;

    private pageTitleSpan: ElementWrapper;
    private usernameLabel: ElementWrapper;
    private logoutLink: ElementWrapper;

    private constructor() {
        super();
        this.pageTitleSpan = new ElementWrapper(this.browserWrapper, by.xpath(xPath.PAGE_TITLE_SPAN));
        this.usernameLabel = new ElementWrapper(this.browserWrapper, by.xpath(xPath.USER_ADDRESS));
        this.logoutLink = new ElementWrapper(this.browserWrapper, by.xpath(xPath.LOGOUT_LINK));
    }

    public static getInstance(): TestManagementPage {
        return new TestManagementPage();
    }

    public async getPageTitle(timeout?: number): Promise<string | null> {
        try {
            await this.pageTitleSpan.waitForVisible(timeout);
            return await this.pageTitleSpan.getText();
        } catch (err) {
            throw new exceptions.RuntimeError(this.getPageTitle, err.message);
        }
    }

    public async getUserName(timeout?: number): Promise<string | null> {
        try {
            await this.usernameLabel.waitForVisible(timeout);
            return await this.usernameLabel.getText();
        } catch (err) {
            throw new exceptions.RuntimeError(this.getUserName, err.message);
        }
    }

    public async logout(): Promise<void> {
        try {
            let flag: boolean = await this.checkPageDisplayed();
            if (flag == true) {
                Logger.debug(LogTag.UI_ACTION, "Logout from the current account");
                await this.usernameLabel.leftClick();
                await this.logoutLink.leftClick();
            }
        } catch (err) {
            throw new exceptions.RuntimeError(this.logout, err.message);
        }
    }

    public async checkPageDisplayed(timeout?: number): Promise<boolean> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Check management test page displayed");
            return await this.getPageTitle(timeout) == PageTitle.MANAGEMENT_TEST;
        } catch (err) {
            return Promise.resolve(false);
        }
    }
}