import { BasePage } from "./base-page";
import { ElementWrapper } from "@utilities/protractor/element-wrapper";
import { by } from "protractor";
import { Logger, LogTag } from "@utilities/general/logger";
import { exceptions } from "@utilities/general/exceptions";
import { Timeout } from "@test-data/timeout";

let XPath = {
    LOADING_IC: '//div[@id="loading-screen-container"]',
    TOAST_LABEL: '//div[@id="toast-container"]/div[@toast-component][1]/div[@class="toast-message ng-star-inserted"]'
}

export abstract class LobbyBasePage extends BasePage {

    protected loadingIcon: ElementWrapper;
    protected toastLabel: ElementWrapper;

    constructor() {
        super();
        this.loadingIcon = new ElementWrapper(this.browserWrapper, by.xpath(XPath.LOADING_IC));
        this.toastLabel = new ElementWrapper(this.browserWrapper, by.xpath(XPath.TOAST_LABEL));
    }

    public async getToastMessage(): Promise<string> {
        try {
            await this.toastLabel.waitForVisible();
            let msg: string = await this.toastLabel.getText();
            Logger.debug(LogTag.UI_ACTION, `Get toast message: "${msg}"`);
            await this.toastLabel.waitForInvisible();
            return msg;
        } catch (err) {
            throw new exceptions.RuntimeError(this.getToastMessage, err.message);
        }
    }

    public async waitForLoadingVisible(timeout?: number): Promise<void> {
        try {
            await this.loadingIcon.waitForVisible(timeout);
        } catch (err) {
            throw new exceptions.RuntimeError(this.waitForLoadingVisible, err.message);
        }
    }

    public async waitForLoadingInvisible(timeout?: number): Promise<void> {
        try {
            await this.loadingIcon.waitForInvisible(timeout);
        } catch (err) {
            throw new exceptions.RuntimeError(this.waitForLoadingInvisible, err.message);
        }
    }

    public async isLoadingDisplayed(timeout: number = Timeout.SHORTIME_DISPLAY_TIMEOUT): Promise<boolean> {
        try {
            await this.loadingIcon.waitForInvisible(timeout);
            return true;
        } catch (err) {
            return false;
        }
    }
}