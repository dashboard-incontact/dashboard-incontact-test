import { ElementWrapper } from "@utilities/protractor/element-wrapper"
import { by } from "protractor"
import { PageURL } from '@test-data/page-url'
import { PageTitle } from "@test-data/page-title"
import { Logger, LogTag } from "@utilities/general/logger"
import { LobbyBasePage } from "./lobby-base-page"
import { GmailMessage, GmailAccount } from "@utilities/api/gmail/gmail-api-type"
import { GmailChecker } from "@utilities/api/gmail/gmail-checker"
import { ResetPasswordEmail, ResetPasswordEmailFilter } from "@data-objects/reset-password-email"
import { exceptions } from "@utilities/general/exceptions"

let xPath = {
    EMAIL_TXT: '//input[@id="Email"]',
    CANCEL_BTN: '//div[@class="card-body pb-4"]//button[@type="button"]',
    SUBMIT_BTN: '//div[@class="card-body pb-4"]//button[@type="submit"]',
    RESET_PASSWORD_LBL: '//div[@class="card-body text-center font-weight-bold align-middle form-header"]/span'
}

export class ResetPasswordPage extends LobbyBasePage {

    public readonly url: string = PageURL.FORGOT_PASSWORD

    protected emailTxt: ElementWrapper;
    protected cancelBtn: ElementWrapper;
    protected submitBtn: ElementWrapper;
    protected resetPasswordLbl: ElementWrapper;

    constructor() {
        super();
        this.emailTxt = new ElementWrapper(this.browserWrapper, by.xpath(xPath.EMAIL_TXT));
        this.cancelBtn = new ElementWrapper(this.browserWrapper, by.xpath(xPath.CANCEL_BTN));
        this.submitBtn = new ElementWrapper(this.browserWrapper, by.xpath(xPath.SUBMIT_BTN));
        this.resetPasswordLbl = new ElementWrapper(this.browserWrapper, by.xpath(xPath.RESET_PASSWORD_LBL));
    }

    public static getInstance(): ResetPasswordPage {
        return new ResetPasswordPage();
    }

    public async checkPageDisplayed(timeout?: number): Promise<boolean> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Check reset password page displayed");
            await this.resetPasswordLbl.waitForVisible(timeout);
            return await this.resetPasswordLbl.getText() == PageTitle.RESET_PASSWORD;
        } catch (err) {
            return Promise.resolve(false);
        }
    }

    public async clearEmailTextbox(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Clear email textbox");
            await this.emailTxt.clear();
        } catch (err) {
            throw new exceptions.RuntimeError(this.clearEmailTextbox, err.message);
        }
    }

    public async enterEmail(email: string): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, `Enter email "${email}"`);
            await this.emailTxt.clear();
            await this.emailTxt.enterKeys(email);
        } catch (err) {
            throw new exceptions.RuntimeError(this.enterEmail, err.message);
        }
    }

    public async clickSubmitButton(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Click submit button");
            await this.submitBtn.leftClick();
        } catch (err) {
            throw new exceptions.RuntimeError(this.clickSubmitButton, err.message);
        }
    }

    public async checkResetPasswordEmail(username: string, gmailAccount: GmailAccount): Promise<string> {
        try {
            let gmailChecker: GmailChecker = new GmailChecker(username, gmailAccount);
            let emails: GmailMessage[] | undefined = await gmailChecker.scheduleEmailCheckingTask(new ResetPasswordEmailFilter());
            if (emails && emails.length > 0) {
                let resetPasswordEmail: ResetPasswordEmail = new ResetPasswordEmail(emails[0]);
                return resetPasswordEmail.getNewPassword();
            } else {
                throw new Error("Cannot find the reset password email");
            }
        } catch (err) {
            throw new exceptions.RuntimeError(this.checkResetPasswordEmail, err.message);
        }
    }
}