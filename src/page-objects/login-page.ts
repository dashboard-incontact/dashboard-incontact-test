import { by } from "protractor";
import { ElementWrapper } from "@utilities/protractor/element-wrapper";
import { UserDataObject } from "@data-objects/user";
import { PageURL } from '@test-data/page-url';
import { Logger, LogTag } from "@utilities/general/logger";
import { PageTitle } from "@test-data/page-title";
import { LobbyBasePage } from "./lobby-base-page";
import { exceptions } from "@utilities/general/exceptions";

let xPath = {
    USERNAME_TXT: '//input[@id="Username"]',
    PASSWORD_TXT: '//input[@id="Password"]',
    LOGIN_BTN: '//button[@id="logIn"]',
    REMEMBER_ME_CHK: `//label[@class='custom-control-label']`,
    LOGIN_SPAN: '//div[@class="card login-form mx-auto shadow"]/div[@class="card-body text-center font-weight-bold align-middle form-header"]/span',
    RESET_PASSWORD_LINK: '//button[@id="forgotPasswordLink"]',
    PAGE_TXT: `//p[@class='font']`,
    REGISTER_LINK: `//button[contains(text(),"Don't have an account? Click here.")]`
}

export class LoginPage extends LobbyBasePage {

    protected url: string = PageURL.LOGIN;

    protected usernameTxt: ElementWrapper;
    protected passwordTxt: ElementWrapper;
    protected loginBtn: ElementWrapper;
    protected rememberMeChk: ElementWrapper;
    protected loginSpan: ElementWrapper;
    protected resetPasswordLink: ElementWrapper;
    protected pageTxt: ElementWrapper;
    protected registerLink: ElementWrapper;

    private constructor() {
        super();
        this.usernameTxt = new ElementWrapper(this.browserWrapper, by.xpath(xPath.USERNAME_TXT));
        this.passwordTxt = new ElementWrapper(this.browserWrapper, by.xpath(xPath.PASSWORD_TXT));
        this.loginBtn = new ElementWrapper(this.browserWrapper, by.xpath(xPath.LOGIN_BTN));
        this.rememberMeChk = new ElementWrapper(this.browserWrapper, by.xpath(xPath.REMEMBER_ME_CHK));
        this.loginSpan = new ElementWrapper(this.browserWrapper, by.xpath(xPath.LOGIN_SPAN));
        this.resetPasswordLink = new ElementWrapper(this.browserWrapper, by.xpath(xPath.RESET_PASSWORD_LINK));
        this.pageTxt = new ElementWrapper(this.browserWrapper, by.xpath(xPath.PAGE_TXT));
        this.registerLink = new ElementWrapper(this.browserWrapper, by.xpath(xPath.REGISTER_LINK));
    }

    public static getInstance(): LoginPage {
        return new LoginPage();
    }

    public async enterUsername(username: string): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, `Enter username "${username}"`);
            await this.usernameTxt.clear();
            await this.usernameTxt.enterKeys(username);
        } catch (err) {
            throw new exceptions.RuntimeError(this.enterUsername, err.message);
        }
    }

    public async enterPassword(password: string): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, `Enter password "${password}"`);
            await this.passwordTxt.clear();
            await this.passwordTxt.enterKeys(password);
        } catch (err) {
            throw new exceptions.RuntimeError(this.enterPassword, err.message);
        }
    }

    public async enterLoginForm(userData: UserDataObject): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, `Enter username "${userData.username}" and password "${userData.password}"`);
            await this.usernameTxt.clear();
            await this.usernameTxt.enterKeys(userData.username);
            await this.passwordTxt.clear();
            await this.passwordTxt.enterKeys(userData.password);
        } catch (err) {
            throw new exceptions.RuntimeError(this.enterLoginForm, err.message);
        }
    }

    public async clickLoginButton(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Click login button");
            await this.loginBtn.leftClick();
            if (await this.isLoadingDisplayed()) {
                await this.loadingIcon.waitForInvisible();
            }
        } catch (err) {
            throw new exceptions.RuntimeError(this.clickLoginButton, err.message);
        }
    }

    public async checkPageDisplayed(timeout?: number): Promise<boolean> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Check login page displayed");
            await this.loginSpan.waitForVisible(timeout);
            return await this.loginSpan.getText() == PageTitle.LOGIN;
        } catch (err) {
            return Promise.resolve(false);
        }
    }

    public async clickOnResetPasswordLink(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Click on Reset Password link");
            await this.resetPasswordLink.leftClick();
        } catch (err) {
            throw new exceptions.RuntimeError(this.clickOnResetPasswordLink, err.message);
        }
    }

    public async isRememberMeChecked(): Promise<boolean> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Check Remember me checkbox");
            return await this.rememberMeChk.isSelected();
        } catch (err) {
            throw new exceptions.RuntimeError(this.isRememberMeChecked, err.message);
        }
    }

    public async clearUsernameTextbox(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Clear Username textbox");
            await this.usernameTxt.clear();
        } catch (err) {
            throw new exceptions.RuntimeError(this.clearUsernameTextbox, err.message);
        }
    }

    public async clearPasswordTextbox(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Clear Password textbox");
            await this.passwordTxt.clear();
        } catch (err) {
            throw new exceptions.RuntimeError(this.clearPasswordTextbox, err.message);
        }
    }

    public async clickRememberBtn(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Click Remember me button");
            await this.rememberMeChk.leftClick();
        } catch (err) {
            throw new exceptions.RuntimeError(this.clickRememberBtn, err.message);
        }
    }

    public async getPageTitle(): Promise<string | null> {
        try {
            await this.pageTxt.waitForVisible();
            return await this.pageTxt.getText();
        } catch (err) {
            throw new exceptions.RuntimeError(this.getPageTitle, err.message);
        }
    }

    public async clickRegisterLink(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Click register hyperlink");
            await this.registerLink.leftClick();
        } catch (err) {
            throw new exceptions.RuntimeError(this.clickRegisterLink, err.message);
        }
    }
}