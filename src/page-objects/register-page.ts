import { RegisterDataObject } from "@data-objects/register";
import { UserDataObject } from "@data-objects/user";
import { PageTitle } from "@test-data/page-title";
import { PageURL } from "@test-data/page-url";
import { Logger, LogTag } from "@utilities/general/logger";
import { Utility } from "@utilities/general/utility";
import { ElementWrapper } from "@utilities/protractor/element-wrapper";
import { by } from "protractor";
import { LobbyBasePage } from "./lobby-base-page";
import { GmailMessage, GmailAccount } from "@utilities/api/gmail/gmail-api-type";
import { GmailChecker } from "@utilities/api/gmail/gmail-checker";
import { RegisterEmail, RegisterEmailFilter } from "@data-objects/register-email";
import { exceptions } from "@utilities/general/exceptions";

let xPath = {
    USERNAME_TXT: `//input[@id='Id']`,
    EMAIL_TXT: `//input[@id='Email']`,
    PASSWORD_TXT: `//input[@id='Password']`,
    CONFIRMPASSWORD_TXT: `//input[@id='ConfirmPass']`,
    SIGNUP_BTN: `//button[@type="submit"]`,
    REGISTER_TITLE_SPAN: '//div[@class="container text-center signup-form"]//div[@class="card-body text-center text-capitalize font-weight-bold align-middle form-header"]/span',
    SUCCESSFULLY_MESSAGE: `//div[@id="toast-container"]/div[@toast-component][2]/div[@class="toast-message ng-star-inserted"]`,
    EMAIL_SENT_MESSAGE: `//div[@id="toast-container"]/div[@toast-component][1]/div[@class="toast-message ng-star-inserted"]`,
    LOGIN_LINK: `//button[@class='btn btn-link text-center']`
}

export class RegisterPage extends LobbyBasePage {

    protected url: string = PageURL.REGISTER;

    protected usernameTxt: ElementWrapper;
    protected emailTxt: ElementWrapper;
    protected passwordTxt: ElementWrapper;
    protected confirmPasswordTxt: ElementWrapper;
    protected signupBtn: ElementWrapper;
    protected registerTitleSpn: ElementWrapper;
    protected successfullyMessage: ElementWrapper;
    protected emailSentMessage: ElementWrapper;
    protected logInLink: ElementWrapper;

    private constructor() {
        super();
        this.usernameTxt = new ElementWrapper(this.browserWrapper, by.xpath(xPath.USERNAME_TXT));
        this.emailTxt = new ElementWrapper(this.browserWrapper, by.xpath(xPath.EMAIL_TXT));
        this.passwordTxt = new ElementWrapper(this.browserWrapper, by.xpath(xPath.PASSWORD_TXT));
        this.confirmPasswordTxt = new ElementWrapper(this.browserWrapper, by.xpath(xPath.CONFIRMPASSWORD_TXT));
        this.signupBtn = new ElementWrapper(this.browserWrapper, by.xpath(xPath.SIGNUP_BTN));
        this.registerTitleSpn = new ElementWrapper(this.browserWrapper, by.xpath(xPath.REGISTER_TITLE_SPAN));
        this.successfullyMessage = new ElementWrapper(this.browserWrapper, by.xpath(xPath.SUCCESSFULLY_MESSAGE));
        this.emailSentMessage = new ElementWrapper(this.browserWrapper, by.xpath(xPath.EMAIL_SENT_MESSAGE));
        this.logInLink = new ElementWrapper(this.browserWrapper, by.xpath(xPath.LOGIN_LINK));
    }

    public static async getInstance(): Promise<RegisterPage> {
        let page: RegisterPage = new RegisterPage();
        await page.waitForLoadingInvisible();
        return page;
    }

    public async enterRegisterForm(registerData: RegisterDataObject): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, `Enter username "${registerData.username}", email "${registerData.email}", password "${registerData.password}" and confirmpassword "${registerData.confirmPassword}"`);
            await this.usernameTxt.clear();
            await this.usernameTxt.enterKeys(registerData.username);
            await this.emailTxt.clear();
            await this.emailTxt.enterKeys(registerData.email);
            await this.passwordTxt.clear();
            await this.passwordTxt.enterKeys(registerData.password);
            await this.confirmPasswordTxt.clear();
            await this.confirmPasswordTxt.enterKeys(registerData.confirmPassword);
        } catch (err) {
            throw new exceptions.RuntimeError(this.enterRegisterForm, err.message);
        }
    }

    public async clickSignUpButton(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Click sign up button");
            await this.signupBtn.leftClick();
            if (await this.isLoadingDisplayed()) {
                await this.loadingIcon.waitForInvisible();
            }
        } catch (err) {
            throw new exceptions.RuntimeError(this.clickSignUpButton, err.message);
        }
    }

    public async checkPageDisplayed(timeout?: number): Promise<boolean> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Check register page displayed");
            await this.registerTitleSpn.waitForVisible(timeout);
            return await this.registerTitleSpn.getText() == PageTitle.REGISTER;
        } catch (err) {
            return Promise.resolve(false);
        }
    }

    public async fillFormWithRandomData(usernameLen: number, emailLen: number, passwordLen: number): Promise<void> {
        try {
            let randomPass: string = Utility.getRandomStr(passwordLen);
            let registerData: RegisterDataObject = new RegisterDataObject(Utility.getRandomStr(usernameLen),
                Utility.getRandomValidEmail(emailLen), randomPass, randomPass);
            await this.enterRegisterForm(registerData);
        } catch (err) {
            throw new exceptions.RuntimeError(this.fillFormWithRandomData, err.message);
        }
    }

    public async fillFormWithEmail(email: string): Promise<void> {
        try {
            let randomPass: string = Utility.getRandomStr(10);
            let registerData: RegisterDataObject = new RegisterDataObject(Utility.getRandomStr(10),
                email, randomPass, randomPass);
            await this.enterRegisterForm(registerData);
        } catch (err) {
            throw new exceptions.RuntimeError(this.fillFormWithEmail, err.message);
        }
    }

    public async verifyErrorMessageForInvalidEmails(invalidEmails: any[], expectedErrorMessage: string): Promise<boolean> {
        try {
            let passed: boolean = true;
            await Utility.allOf(invalidEmails, async (element: any) => {
                await this.fillFormWithEmail(element.email);
                await this.clickSignUpButton();
                let errMsg: string = await this.getToastMessage();
                if (errMsg != expectedErrorMessage) {
                    passed = false;
                }
            })
            return passed;
        } catch (err) {
            throw new exceptions.RuntimeError(this.verifyErrorMessageForInvalidEmails, err.message);
        }
    }

    public async clearUsernameTextbox(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Clear Username textbox");
            await this.usernameTxt.clear();
        } catch (err) {
            throw new exceptions.RuntimeError(this.clearUsernameTextbox, err.message);
        }
    }

    public async clearEmailTextbox(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Clear Email textbox");
            await this.emailTxt.clear();
        } catch (err) {
            throw new exceptions.RuntimeError(this.clearEmailTextbox, err.message);
        }
    }

    public async clearPasswordTextbox(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Clear Password textbox");
            await this.passwordTxt.clear();
        } catch (err) {
            throw new exceptions.RuntimeError(this.clearPasswordTextbox, err.message);
        }
    }

    public async clearConfirmPasswordTextbox(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Clear ConfirmPassword textbox");
            await this.confirmPasswordTxt.clear();
        } catch (err) {
            throw new exceptions.RuntimeError(this.clearConfirmPasswordTextbox, err.message);
        }
    }

    public async clearDataTextbox(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Clear data (if any) in all the text boxes");
            await this.usernameTxt.clear();
            await this.emailTxt.clear();
            await this.passwordTxt.clear();
            await this.confirmPasswordTxt.clear();
        } catch (err) {
            throw new exceptions.RuntimeError(this.clearDataTextbox, err.message);
        }
    }

    public async registerNewRandomAccount(email: string): Promise<UserDataObject> {
        try {
            Logger.debug(LogTag.UI_ACTION, 'Register a radom user by using input email');
            let password: string = Utility.getRandomStr(10);
            let randomRegister: RegisterDataObject = new RegisterDataObject("test" + Utility.getRandomStr(10), email, password, password);
            await this.enterRegisterForm(randomRegister);
            await this.clickSignUpButton();
            await this.emailSentMessage.waitForVisible();
            await this.successfullyMessage.waitForVisible();
            await this.emailSentMessage.waitForInvisible();
            await this.successfullyMessage.waitForInvisible();
            return new UserDataObject(randomRegister.username, randomRegister.password, randomRegister.email);
        } catch (err) {
            throw new exceptions.RuntimeError(this.registerNewRandomAccount, err.message);
        }
    }

    public async checkRegistrationSuccessfullyEmail(username: string, gmailAccount: GmailAccount): Promise<string> {
        try {
            let gmailChecker: GmailChecker = new GmailChecker(username, gmailAccount);
            let email: GmailMessage[] | undefined = await gmailChecker.scheduleEmailCheckingTask(new RegisterEmailFilter());
            if (email && email.length > 0) {
                let registerEmail: RegisterEmail = new RegisterEmail(email[0]);
                return registerEmail.getUsername();
            } else {
                throw new Error("Cannot find the successful registration email");
            }
        } catch (err) {
            throw new exceptions.RuntimeError(this.checkRegistrationSuccessfullyEmail, err.message);
        }
    }

    public async verifyRegistrationSuccessfullyMessages(success: string, email: string): Promise<boolean> {
        try {
            await this.emailSentMessage.waitForVisible();
            await this.successfullyMessage.waitForVisible();
            let msg1: string = await this.successfullyMessage.getText();
            Logger.debug(LogTag.UI_ACTION, `Get toast message: "${msg1}"`);
            let msg2: string = await this.emailSentMessage.getText();
            Logger.debug(LogTag.UI_ACTION, `Get toast message: "${msg2}"`);
            let passed: boolean = false;
            if ((msg1 == success) && (msg2 == email)) {
                passed = true;
            }
            else {
                passed = false;
            };
            await this.emailSentMessage.waitForInvisible();
            await this.successfullyMessage.waitForInvisible();
            return passed;
        } catch (err) {
            throw new exceptions.RuntimeError(this.verifyRegistrationSuccessfullyMessages, err.message);
        }
    }

    public async clickLogInLink(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Click log in hyperlink");
            await this.logInLink.leftClick();
        } catch (err) {
            throw new exceptions.RuntimeError(this.clickLogInLink, err.message);
        }
    }
}