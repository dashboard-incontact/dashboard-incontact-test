import { BrowserManager } from "@utilities/protractor/browser-manager";
import { BrowserWrapper } from "@utilities/protractor/browser-wrapper";
import { exceptions } from "@utilities/general/exceptions";

export abstract class BasePage {

    protected abstract url: string;
    protected browserWrapper: BrowserWrapper;

    constructor() {
        this.browserWrapper = BrowserManager.getCurrentBrowser();
    }

    /**
     * Check if a page is presented
     * This method should return false when timeout
     * 
     * @param timeout maximum time for waiting elements
     */
    public abstract async checkPageDisplayed(timeout?: number): Promise<boolean>

    /**
     * Return the browser title bar text
     */
    public async getTitleBarText(): Promise<string> {
        try {
            return await this.browserWrapper.getTitleBarText();
        } catch (err) {
            throw new exceptions.RuntimeError(this.getTitleBarText, err.message);
        }
    }
}