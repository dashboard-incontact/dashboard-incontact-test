import { Logger, LogTag } from "@utilities/general/logger";
import { BrowserManager } from "@utilities/protractor/browser-manager";
import { PageURL } from "@test-data/page-url";
import { LoginPage } from "@page-objects/login-page";
import { userList } from "@test-data/user-data";
import { TestManagementPage } from "@page-objects/test-management";
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers";

describe('DB_LOB_LOGIN-009', async () => {

    let loginPage: LoginPage;
    let testManagementPage: TestManagementPage;
    let validUser = userList.user1;

    beforeEach(async () => {
        Logger.force(LogTag.TESTCASE, 'DB_LOB_LOGIN-009 Verify that the AUT redirects to "Login" page when user goes back after logging out');
        jasmine.addMatchers(CompareCustomMatchers);
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
    });

    it('Verify that the AUT redirects to "Login" page when user goes back after logging out', async () => {
        // 1. Navigate to the "Dashboard" website
        loginPage = LoginPage.getInstance();

        // 2. Enter the valid username and password
        await loginPage.enterLoginForm(validUser);

        // 3. Click on the "Login" button
        await loginPage.clickLoginButton();

        // 4. Log out from the current account
        testManagementPage = TestManagementPage.getInstance();
        await testManagementPage.logout();
        loginPage = LoginPage.getInstance();
        expect(await loginPage.checkPageDisplayed()).toBeTruthy('The Login page is not displayed');

        // 5. Click on back button of the web browser
        await BrowserManager.getCurrentBrowser().goBack();
        expect(await loginPage.checkPageDisplayed()).toBeTruthy('The Login page is not displayed');
    });
});