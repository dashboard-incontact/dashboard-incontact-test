import { LoginPage } from "@page-objects/login-page";
import { BrowserManager } from "@utilities/protractor/browser-manager";
import { Messages } from "@test-data/messages";
import { Logger, LogTag } from "@utilities/general/logger";
import { PageURL } from "@test-data/page-url";
import { userList } from "@test-data/user-data";
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers";

describe('DB_LOB_LOGIN-003', async () => {

    let loginPage: LoginPage;
    let validUser = userList.user2;

    beforeEach(async () => {
        jasmine.addMatchers(CompareCustomMatchers);
        Logger.force(LogTag.TESTCASE, 'DB_LOB_LOGIN-003 Verify that an error message displays when user login with missing required field(s)')
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
    });

    it('Verify that an error message displays when user login with missing required field(s)', async () => {
        // 1. Navigate to "Login" page
        loginPage = LoginPage.getInstance();

        // 2. Fill in "Username" textbox with valid information (e.g. nam.nguyen)
        await loginPage.enterUsername(validUser.username);

        // 3. Leave the "Password" textbox empty
        await loginPage.clearPasswordTextbox();

        // 4. Click "Login" button
        await loginPage.clickLoginButton();
        expect(await loginPage.getToastMessage()).toEqual(Messages.EMPTY_USERNAME_OR_PASSWORD, 'The error message for empty username or password is not as expected');

        // 5. Clear the data (if any) in the "Username" and "Password" textboxes
        // 6. Leave the "Username" textbox empty
        await loginPage.clearUsernameTextbox();

        // 7. Fill in "Password" textbox with valid information (e.g. inContact4ever)
        await loginPage.enterPassword(validUser.password);

        // 8. Click "Login" button
        await loginPage.clickLoginButton();
        expect(await loginPage.getToastMessage()).toEqual(Messages.EMPTY_USERNAME_OR_PASSWORD, 'The error message for empty username or password is not as expected');

        // 9. Clear the data (if any) in the "Username" and "Password" textboxes 
        // 10. Leave username and password textboxes empty
        await loginPage.clearPasswordTextbox();

        // 11. Click "Login" button
        await loginPage.clickLoginButton();
        expect(await loginPage.getToastMessage()).toEqual(Messages.EMPTY_USERNAME_OR_PASSWORD, 'The error message for empty username or password is not as expected');
    });
});