import { LoginPage } from "@page-objects/login-page"
import { ResetPasswordPage } from "@page-objects/reset-password-page"
import { Messages } from "@test-data/messages"
import { PageURL } from "@test-data/page-url"
import { Logger, LogTag } from "@utilities/general/logger"
import { Utility } from "@utilities/general/utility"
import { BrowserManager } from "@utilities/protractor/browser-manager"
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers"

describe('DB_LOB_RP-004', async () => {

    let loginPage: LoginPage;
    let resetPasswordPage: ResetPasswordPage;

    beforeEach(async () => {
        Logger.force(LogTag.TESTCASE, 'DB_LOB_RP-004 Verify that an error message displays when submitting an invalid email address on the "Forgot Password" page');
        jasmine.addMatchers(CompareCustomMatchers);
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
        loginPage = LoginPage.getInstance();
        await loginPage.clickOnResetPasswordLink();
    });

    it('Verify that an error message displays when submitting an invalid email address on the "Forgot Password" page', async () => {
        // 1. Navigate to the "Forgot Password" page 
        resetPasswordPage = ResetPasswordPage.getInstance();

        // 2. Leave the "Your Email" textbox with empty value
        await resetPasswordPage.clearEmailTextbox();

        // 3. Click on the "Submit" button
        await resetPasswordPage.clickSubmitButton();
        expect(await resetPasswordPage.getToastMessage()).toEqual(Messages.EMPTY_EMAIL, "The error message of invalid email is not as expected");

        // 4. Enter an invalid email address into "Your Email" textbox (ex: "qwerty@abc", "!@#$%^&*()_+", "abc.com.vn", "abcdef", "@gmail.com")
        await resetPasswordPage.enterEmail(Utility.getRandomStr(10));

        // 5. Click on the "Submit" button
        await resetPasswordPage.clickSubmitButton();

        // VP Verify the toast message
        expect(await resetPasswordPage.getToastMessage()).toEqual(Messages.INVALID_EMAIL, "The error message of invalid email is not as expected");
    });
});