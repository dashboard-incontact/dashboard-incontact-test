import { RegisterPage } from "@page-objects/register-page";
import { Logger, LogTag } from "@utilities/general/logger";
import { BrowserManager } from "@utilities/protractor/browser-manager";
import { PageURL } from "@test-data/page-url";
import { RegisterDataObject } from "@data-objects/register";
import { registerList } from "@test-data/register-data";
import { Messages } from "@test-data/messages";
import { LoginPage } from "@page-objects/login-page";
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers";

describe('DB_LOB_REG-001', async () => {

    let loginPage: LoginPage;
    let registerPage: RegisterPage;
    let registerInfo: RegisterDataObject = RegisterDataObject.initRegister(registerList.emptyFieldRegister);

    beforeEach(async () => {
        Logger.force(LogTag.TESTCASE, 'DB_LOB_REG-001 Verify that an error message displays when registering new account with missing mandatory field(s).');
        jasmine.addMatchers(CompareCustomMatchers);
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
        loginPage = LoginPage.getInstance();
        loginPage.clickRegisterLink();
    });

    it('Verify that an error message displays when registering new account with missing mandatory field(s).', async () => {
        // 1. Navigate to Register page 
        registerPage = await RegisterPage.getInstance();

        // 2. Leave "Username" textbox empty
        // 3. Fill the remain textboxes with valid data
        await registerPage.enterRegisterForm(registerInfo);
        await registerPage.clearUsernameTextbox();

        // 4. Click "Sign up" button
        await registerPage.clickSignUpButton();

        // VP Verify error message 
        expect(await registerPage.getToastMessage()).toEqual(Messages.PLEASE_INPUT_INFORMATION, `Error message of please input information is not as expected`);

        // 5. Clear data (if any) in all the textboxes
        await registerPage.clearDataTextbox();

        // 6. Leave "Email" textbox empty 
        // 7. Fill the remain textboxes with valid data 
        await registerPage.enterRegisterForm(registerInfo);
        await registerPage.clearEmailTextbox();

        // 8. Click "Sign up" button
        await registerPage.clickSignUpButton();

        // VP Verify error message 
        expect(await registerPage.getToastMessage()).toEqual(Messages.THE_EMAIL_IS_NOT_VALID, `The error message of invalid email is not as expected`);

        // 9. Clear data (if any) in all the textboxes
        await registerPage.clearDataTextbox();

        // 10. Leave "Password" textbox empty
        // 11. Fill the remain textboxes with valid data 
        await registerPage.enterRegisterForm(registerInfo);
        await registerPage.clearPasswordTextbox();

        // 12. Click "Sign up" button
        await registerPage.clickSignUpButton();

        // VP Verify error message 
        expect(await registerPage.getToastMessage()).toEqual(Messages.PLEASE_INPUT_INFORMATION, `The error message of input information is not as expected`);

        // 13. Clear data (if any) in all the 
        await registerPage.clearDataTextbox();

        // 14. Leave "Confirm Password" textbox empty
        // 15. Fill the remain textboxes with valid data
        await registerPage.enterRegisterForm(registerInfo);
        await registerPage.clearConfirmPasswordTextbox();

        // 16. Click "Sign up" button
        await registerPage.clickSignUpButton();

        // VP Verify error message 
        expect(await registerPage.getToastMessage()).toEqual(Messages.PLEASE_INPUT_INFORMATION, `The error message of input information is not as expected`);
    });
});