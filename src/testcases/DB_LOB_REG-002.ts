import { Logger, LogTag } from "@utilities/general/logger";
import { BrowserManager } from "@utilities/protractor/browser-manager";
import { PageURL } from "@test-data/page-url";
import { RegisterPage } from "@page-objects/register-page";
import { Messages } from "@test-data/messages";
import { LoginPage } from "@page-objects/login-page";
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers";

describe('DB_LOB_REG-002', async () => {

    let loginPage: LoginPage;
    let registerPage: RegisterPage;

    beforeEach(async () => {
        Logger.force(LogTag.TESTCASE, 'DB_LOB_REG-002 Verify that an error message displays when user registers with invalid data length');
        jasmine.addMatchers(CompareCustomMatchers);
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
        loginPage = LoginPage.getInstance();
        loginPage.clickRegisterLink();
    });

    it('Verify that an error message displays when user registers with invalid data length.', async () => {
        // 1. Navigate to Register page
        registerPage = await RegisterPage.getInstance();

        // 2. Fill in "Username" textbox with invalid data length (e.g. data with 3, 7, 46, 70 characters)
        // 3. Fill the remain textboxes with valid data
        // 4. Click "Sign up" button
        await registerPage.fillFormWithRandomData(3, 15, 10);
        await registerPage.clickSignUpButton();
        expect(await registerPage.getToastMessage()).toEqual(Messages.INVALID_USERNAME_LENGTH, "The error message for invalid username length is not as expected");

        await registerPage.fillFormWithRandomData(7, 15, 10);
        await registerPage.clickSignUpButton();
        expect(await registerPage.getToastMessage()).toEqual(Messages.INVALID_USERNAME_LENGTH, "The error message for invalid username length is not as expected");

        await registerPage.fillFormWithRandomData(46, 15, 10);
        await registerPage.clickSignUpButton();
        expect(await registerPage.getToastMessage()).toEqual(Messages.INVALID_USERNAME_LENGTH, "The error message for invalid username length is not as expected");

        await registerPage.fillFormWithRandomData(70, 15, 10);
        await registerPage.clickSignUpButton();
        expect(await registerPage.getToastMessage()).toEqual(Messages.INVALID_USERNAME_LENGTH, "The error message for invalid username length is not as expected");

        // 5. Repeat from step 2 to 4 with "Password" textbox instead of "Username" textbox
        await registerPage.fillFormWithRandomData(10, 15, 3);
        await registerPage.clickSignUpButton();
        expect(await registerPage.getToastMessage()).toEqual(Messages.INVALID_PASSWORD_LENGTH, "The error message for invalid password length is not as expected");

        await registerPage.fillFormWithRandomData(10, 15, 7);
        await registerPage.clickSignUpButton();
        expect(await registerPage.getToastMessage()).toEqual(Messages.INVALID_PASSWORD_LENGTH, "The error message for invalid password length is not as expected");

        await registerPage.fillFormWithRandomData(10, 15, 46);
        await registerPage.clickSignUpButton();
        expect(await registerPage.getToastMessage()).toEqual(Messages.INVALID_PASSWORD_LENGTH, "The error message for invalid password length is not as expected");

        await registerPage.fillFormWithRandomData(10, 15, 70);
        await registerPage.clickSignUpButton();
        expect(await registerPage.getToastMessage()).toEqual(Messages.INVALID_PASSWORD_LENGTH, "The error message for invalid password length is not as expected");
    });
});