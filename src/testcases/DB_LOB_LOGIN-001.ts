import { UserDataObject } from "@data-objects/user";
import { LoginPage } from "@page-objects/login-page";
import { TestManagementPage } from "@page-objects/test-management";
import { PageURL } from "@test-data/page-url";
import { userList } from "@test-data/user-data";
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers";
import { Logger, LogTag } from "@utilities/general/logger";
import { BrowserManager } from "@utilities/protractor/browser-manager";

describe('DB_LOB_LOGIN-001', async () => {

    let loginPage: LoginPage;
    let testManagementPage: TestManagementPage;
    let userData: UserDataObject = UserDataObject.initUser(userList.user1);

    beforeEach(async () => {
        jasmine.addMatchers(CompareCustomMatchers);
        Logger.force(LogTag.TESTCASE, 'DB_LOB_LOGIN-001 Verify that the "Manage Tests" page displays when logging in with a valid account');
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
    });

    it('Verify that the "Manage Tests" page displays when logging in with a valid account', async () => {
        // 1. Navigate to "Dashboard" website
        loginPage = LoginPage.getInstance();

        // VP Check Login page displayed
        expect(await loginPage.checkPageDisplayed()).toBeTruthy('The Login page is not displayed');

        // 2. Enter a valid username and password
        await loginPage.enterLoginForm(userData);

        // 3. Click on "Login" button
        await loginPage.clickLoginButton();

        // 4. Verify the "Management Test" page
        testManagementPage = TestManagementPage.getInstance();

        // VP Check Manage Tests displayed
        expect(await testManagementPage.checkPageDisplayed()).toBeTruthy('The Manage Tests page is not displayed');
        expect(await testManagementPage.getUserName()).toEqual(`Hi, ${userData.username}`, 'The username does not match on Manage Tests page');
    });

    afterEach(async () => {
        Logger.force(LogTag.AFTER_EACH, Logger.lineString);
        testManagementPage = TestManagementPage.getInstance();
        await testManagementPage.logout();
    });
});