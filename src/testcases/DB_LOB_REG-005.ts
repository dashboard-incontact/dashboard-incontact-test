import { RegisterPage } from "@page-objects/register-page";
import { Logger, LogTag } from "@utilities/general/logger";
import { BrowserManager } from "@utilities/protractor/browser-manager";
import { PageURL } from "@test-data/page-url";
import { RegisterDataObject } from "@data-objects/register";
import { registerList } from "@test-data/register-data";
import { Messages } from "@test-data/messages";
import { LoginPage } from "@page-objects/login-page";
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers";

describe('DB_LOB_REG-005', async () => {

    let loginPage: LoginPage;
    let registerPage: RegisterPage;
    let registerMismatchPass: RegisterDataObject = RegisterDataObject.initRegister(registerList.mismatchPass);

    beforeEach(async () => {
        Logger.force(LogTag.TESTCASE, 'DB_LOB_REG-005 Verify that an error message displays when user registers with mismatch password and confirm password.');
        jasmine.addMatchers(CompareCustomMatchers);
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
        loginPage = LoginPage.getInstance();
        loginPage.clickRegisterLink();
    });

    it('Verify that an error message displays when user registers with mismatch password and confirm password.', async () => {
        // 1. Navigate to Register page 
        registerPage = await RegisterPage.getInstance();

        // 2. Fill in "Password" textbox and Confirm "Password" with difference data (e.g. inContact4ever/inContactforever)
        // 3. Fill the remain textboxes with valid data
        await registerPage.enterRegisterForm(registerMismatchPass);

        // 4. Click "Sign up" button
        await registerPage.clickSignUpButton();

        // VP Verify error message 
        expect(await registerPage.getToastMessage()).toEqual(Messages.MISMATCH_PASSWORD, 'The error message for mismatch password is not expected');
    });
});