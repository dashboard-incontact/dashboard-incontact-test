import { LoginPage } from "@page-objects/login-page";
import { PageURL } from "@test-data/page-url";
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers";
import { Logger, LogTag } from "@utilities/general/logger";
import { BrowserManager } from "@utilities/protractor/browser-manager";

describe('DB_LOB_LOGIN-008', async () => {
    let loginPage: LoginPage;

    beforeEach(async () => {
        Logger.force(LogTag.TESTCASE, 'DB_LOB_LOGIN-008 Verify that the AUT redirects to "Login" page when navigating to "Management Test" page without login.');
        jasmine.addMatchers(CompareCustomMatchers);
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
    });

    it('Verify that the AUT redirects to "Login" page when navigating to "Management Test" page without login.', async () => {
        //1. Navigate to the "Management Test" page
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.TEST_MANAGEMENT);

        // VP The "Login" page
        loginPage = LoginPage.getInstance();
        expect(await loginPage.checkPageDisplayed()).toBeTruthy('The Login page is not displayed');
    });
});