import { LoginPage } from "@page-objects/login-page";
import { UserDataObject } from "@data-objects/user";
import { BrowserManager } from "@utilities/protractor/browser-manager";
import { Messages } from "@test-data/messages";
import { Logger, LogTag } from "@utilities/general/logger";
import { PageURL } from "@test-data/page-url";
import { userList } from "@test-data/user-data";
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers";

describe('DB_LOB_LOGIN-002', async () => {

    let loginPage: LoginPage;
    let userData1: UserDataObject = UserDataObject.initUser(userList.user3);
    let userData2: UserDataObject = UserDataObject.initUser(userList.user4);
    let userData3: UserDataObject = UserDataObject.initUser(userList.user5);

    beforeEach(async () => {
        jasmine.addMatchers(CompareCustomMatchers);
        Logger.force(LogTag.TESTCASE, 'DB_LOB_LOGIN-002 Verify that an error message displays when user login with invalid account on Login page.');
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
    });

    it('Verify that an error message displays when user login with invalid account on Login page.', async () => {
        // 1. Navigate to Login page
        loginPage = LoginPage.getInstance();

        // 2. Fill in "Username" and "Password" textboxes with invalid account information (e.g. nam.nguyen/123456789 )
        await loginPage.enterLoginForm(userData1);

        // 3. Click on "Login" button
        await loginPage.clickLoginButton();

        // VP Verify the error message 
        expect(await loginPage.getToastMessage()).toEqual(Messages.INVALID_USERNAME_OR_PASSWORD, `The error message of invalid username or password is not as expected`);

        // 4. Clear the data in the "Username" and "Password" textboxes
        await loginPage.clearUsernameTextbox();
        await loginPage.clearPasswordTextbox();

        // 5. Fill in "Username" and "Password" textboxes with invalid account information (e.g. spaces/spaces)
        await loginPage.enterLoginForm(userData2);

        // 6. Click on "Login" button
        await loginPage.clickLoginButton();

        // VP Verify the error message 
        expect(await loginPage.getToastMessage()).toEqual(Messages.INVALID_USERNAME_OR_PASSWORD, `The error message of invalid username or password is not as expected`);

        // 7. Clear the data in the "Username" and "Password" textboxes
        await loginPage.clearUsernameTextbox();
        await loginPage.clearPasswordTextbox();

        // 8. Fill in "Username" and "Password" textboxes with invalid account information (e.g. 123456789/inContact4ever)
        await loginPage.enterLoginForm(userData3);

        // 9. Click "Login" button
        await loginPage.clickLoginButton();

        // VP Verify the error message 
        expect(await loginPage.getToastMessage()).toEqual(Messages.INVALID_USERNAME_OR_PASSWORD, `The error message of invalid username or password is not as expected`);
    });
});