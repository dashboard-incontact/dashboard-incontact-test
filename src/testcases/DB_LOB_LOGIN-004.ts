import { LoginPage } from "@page-objects/login-page";
import { BrowserManager } from "@utilities/protractor/browser-manager";
import { PageURL } from "@test-data/page-url";
import { Logger, LogTag } from "@utilities/general/logger";
import { userList } from "@test-data/user-data";
import { TestManagementPage } from "@page-objects/test-management";
import { PageTitle } from "@test-data/page-title";
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers";

describe('DB_LOG_LOGIN-004', async () => {

    let loginPage: LoginPage;
    let testManagementPage: TestManagementPage;
    let validUser = userList.user2;

    beforeEach(async () => {
        jasmine.addMatchers(CompareCustomMatchers);
        Logger.force(LogTag.TESTCASE, 'DB_LOB_LOGIN-004 Verify that browser remembers user credentials after user login successfully with "Remember me" option.');
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
    })

    it('Verify that browser remembers user credentials after user login successfully with "Remember me" option.', async () => {
        // 1. Navigate to Login page
        loginPage = LoginPage.getInstance();

        // 2. Make sure "Remember me" check box is selected
        await loginPage.clickRememberBtn();

        // 3. Login with valid account information (e.g. nam.nguyen/inContact4ever)
        await loginPage.enterLoginForm(validUser);
        await loginPage.clickLoginButton();

        // 4. Close browser and open browser again
        await BrowserManager.getCurrentBrowser().restartBrowser();

        // 5. Navigate to Login page
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
        testManagementPage = TestManagementPage.getInstance();

        // 6. Notice the loaded page
        // VP "Manage Tests" page load directly.
        expect(await testManagementPage.getPageTitle()).toEqual(PageTitle.MANAGEMENT_TEST, `The Manage Tests page does not load directly.`);
    });

    afterEach(async () => {
        Logger.force(LogTag.AFTER_EACH, Logger.lineString);
        testManagementPage = TestManagementPage.getInstance();
        await testManagementPage.logout();
    });
});