import { RegisterPage } from "@page-objects/register-page";
import { Logger, LogTag } from "@utilities/general/logger";
import { BrowserManager } from "@utilities/protractor/browser-manager";
import { PageURL } from "@test-data/page-url";
import { Messages } from "@test-data/messages";
import * as invalidEmails from '@test-data/json-data/invalid-email.json';
import { LoginPage } from "@page-objects/login-page";
import { Utility } from "@utilities/general/utility";
import { JsonUtil } from "@utilities/general/json-utility";
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers";

describe("DB_LOB_REG-004", async () => {

    let loginPage: LoginPage;
    let registerPage: RegisterPage;
    let emailList: Array<string> = JsonUtil.mapToArray(invalidEmails, (e: any) => e.email);

    beforeEach(async () => {
        Logger.force(LogTag.TESTCASE, 'DB_LOB_REG-004 Verify that an error message displays when user registers with invalid email address');
        jasmine.addMatchers(CompareCustomMatchers);
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
        loginPage = LoginPage.getInstance();
        loginPage.clickRegisterLink();
    });

    it("Verify that an error message displays when user registers with invalid email address", async () => {
        // 1. Navigate to Register page
        registerPage = await RegisterPage.getInstance();

        // 2. Fill in Email textbox with invalid Email (e.g. 123456789.logigear.com)
        // 3. Fill the remain textboxes with valid data
        await registerPage.fillFormWithEmail(Utility.getRandomElement(emailList));

        // 4. Click "Sign up" button
        await registerPage.clickSignUpButton();

        // VP Verify the toast message
        expect(await registerPage.getToastMessage()).toEqual(Messages.INVALID_EMAIL, "The error message of invalid email is not as expected");
    });
});