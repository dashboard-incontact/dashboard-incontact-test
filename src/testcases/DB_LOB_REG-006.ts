import { Logger, LogTag } from "@utilities/general/logger";
import { BrowserManager } from "@utilities/protractor/browser-manager";
import { PageURL } from "@test-data/page-url";
import { RegisterPage } from "@page-objects/register-page";
import { RegisterDataObject } from "@data-objects/register";
import { Messages } from "@test-data/messages";
import { gmailAccounts } from "@test-data/gmail-account"
import { GmailAccount } from "@utilities/api/gmail/gmail-api-type";
import { LoginPage } from "@page-objects/login-page";
import { UserDataObject } from "@data-objects/user";
import { TestManagementPage } from "@page-objects/test-management";
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers";

describe('DB_LOB_REG-006', async () => {

    let loginPage: LoginPage;
    let testManagementPage: TestManagementPage;
    let gmailAccount: GmailAccount = gmailAccounts.apiTest;
    let registerPage: RegisterPage;
    let randomRegister: RegisterDataObject = RegisterDataObject.getRandomRegister(gmailAccounts.apiTest.email);
    let newRegisterUser: UserDataObject;

    beforeEach(async () => {
        Logger.force(LogTag.TESTCASE, 'DB_LOB_REG-006 Verify that user logs in sucessfully with the account created after registering.');
        jasmine.addMatchers(CompareCustomMatchers);
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.REGISTER);
    });

    it('Verify that user logs in sucessfully with the account created after registering.', async () => {
        // 1. Navigate to Register page
        registerPage = await RegisterPage.getInstance();

        // 2. Fill in "Username", Email, "Password" and "Confirm Password" on the respective textboxes with valid account information
        await registerPage.enterRegisterForm(randomRegister);

        // 3. Click "Sign up" button
        await registerPage.clickSignUpButton();

        // VP Verify the successfull message
        expect(await registerPage.verifyRegistrationSuccessfullyMessages(Messages.SUCCESSFULLY_ACCOUNT_CREATED, Messages.SENT_EMAIL_TO + randomRegister.email + "."))
            .toBe(true, 'The successfull registration message is not as expected');

        // 4. Access to the mail inbox of the email entered in step 2
        expect(await registerPage.checkRegistrationSuccessfullyEmail(randomRegister.username, gmailAccount))
            .toEqual(randomRegister.username, "The username in the email does not match");

        // 5. Navigate to Login page
        // 6. Log in with the new created account
        loginPage = LoginPage.getInstance();
        newRegisterUser = new UserDataObject(randomRegister.username, randomRegister.password, randomRegister.email);
        await loginPage.enterLoginForm(newRegisterUser);
        await loginPage.clickLoginButton();

        // VP Verify the "Manage Tests" page
        testManagementPage = TestManagementPage.getInstance();
        expect(await testManagementPage.checkPageDisplayed()).toBeTruthy('The Manage Tests page does not display');
        expect(await testManagementPage.getUserName()).toEqual(`Hi, ${newRegisterUser.username}`, 'The username displays incorrectly on Test Management page');
    });

    afterEach(async () => {
        Logger.force(LogTag.AFTER_EACH, Logger.lineString);
        testManagementPage = TestManagementPage.getInstance();
        await testManagementPage.logout();
    });
});