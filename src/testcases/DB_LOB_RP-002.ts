import { Logger, LogTag } from "@utilities/general/logger"
import { BrowserManager } from "@utilities/protractor/browser-manager"
import { PageURL } from "@test-data/page-url"
import { ResetPasswordPage } from "@page-objects/reset-password-page"
import { LoginPage } from "@page-objects/login-page"
import { RegisterPage } from "@page-objects/register-page"
import { UserDataObject } from "@data-objects/user"
import { gmailAccounts } from "@test-data/gmail-account"
import { GmailAccount } from "@utilities/api/gmail/gmail-api-type"
import { Utility } from "@utilities/general/utility"
import { Messages } from "@test-data/messages"
import { TestManagementPage } from "@page-objects/test-management"
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers"

describe('DB_LOB_RP-002', async () => {

    let gmailAccount: GmailAccount = gmailAccounts.apiTest;
    let loginPage: LoginPage;
    let registerPage: RegisterPage;
    let resetPasswordPage: ResetPasswordPage;
    let newAccount: UserDataObject;

    beforeEach(async () => {
        Logger.force(LogTag.TESTCASE, 'DB_LOB_RP-002 Verify that an error message displays when logging in with the old password after resetting password successfully')
        jasmine.addMatchers(CompareCustomMatchers);
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.REGISTER);
        registerPage = await RegisterPage.getInstance();
        newAccount = await registerPage.registerNewRandomAccount(Utility.createAliasEmail(gmailAccount.email));
        await BrowserManager.getCurrentBrowser().restartBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.FORGOT_PASSWORD);
    });

    it('Verify that an error message displays when logging in with the old password after resetting password successfully', async () => {
        // 1. Navigate to the "Forgot Password" page 
        resetPasswordPage = ResetPasswordPage.getInstance();

        // 4. Enter the valid email address which is associated to the valid account   
        await resetPasswordPage.enterEmail(newAccount.email);

        // 3. Click on the "Submit" button
        await resetPasswordPage.clickSubmitButton();

        // 4. Access to the mail inbox of the email address given in step 3, find the reset password email and get the new password. 
        await resetPasswordPage.checkResetPasswordEmail(newAccount.username, gmailAccount);

        // 5. Navigate to the "Login" page
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
        loginPage = LoginPage.getInstance();

        // 6. Log in with the old password
        await loginPage.enterLoginForm(newAccount);
        await loginPage.clickLoginButton();

        // VP Verify the toast message
        expect(await loginPage.getToastMessage()).toEqual(Messages.INVALID_USERNAME_OR_PASSWORD, `The error message of invalid username or password is not as expected`);
    });

    afterEach(async () => {
        Logger.force(LogTag.AFTER_EACH, Logger.lineString);
        await TestManagementPage.getInstance().logout();
    });
});