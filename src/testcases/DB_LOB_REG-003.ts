import { RegisterPage } from "@page-objects/register-page";
import { Logger, LogTag } from "@utilities/general/logger";
import { BrowserManager } from "@utilities/protractor/browser-manager";
import { PageURL } from "@test-data/page-url";
import { RegisterDataObject } from "@data-objects/register";
import { registerList } from "@test-data/register-data";
import { Messages } from "@test-data/messages";
import { LoginPage } from "@page-objects/login-page";
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers";

describe('DB_LOB_REG-003', async () => {

    let loginPage: LoginPage;
    let registerPage: RegisterPage;
    let registerDupUsername: RegisterDataObject = RegisterDataObject.initRegister(registerList.duplicateUsernameRegister);
    let registerDupEmail: RegisterDataObject = RegisterDataObject.initRegister(registerList.duplicateEmailRegister);

    beforeEach(async () => {
        Logger.force(LogTag.TESTCASE, 'DB_LOB_REG-003 Verify that an error message displays when user registers with duplicative information.');
        jasmine.addMatchers(CompareCustomMatchers);
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
        loginPage = LoginPage.getInstance();
        loginPage.clickRegisterLink();
    });

    it('Verify that an error message displays when user registers with duplicative information.', async () => {
        // 1. Navigate to Register page 
        registerPage = await RegisterPage.getInstance();

        // 2. Fill in "Username" textbox with duplicate Username 
        // 3. Fill the remain textboxes with valid data
        await registerPage.enterRegisterForm(registerDupUsername);

        // 4. Click "Sign up" button
        await registerPage.clickSignUpButton();

        // VP Verify error message 
        expect(await registerPage.getToastMessage()).toEqual(Messages.USERNAME_IS_USED, `The error message of duplicative username is not as expected`);

        // 5. Clear data (if any) in all the textboxes
        await registerPage.clearDataTextbox();

        // 6. Fill in Email textbox with duplicate Email
        // 7. Fill the remain textboxes with valid data 
        await registerPage.enterRegisterForm(registerDupEmail);

        // 8. Click "Sign up" button
        await registerPage.clickSignUpButton();

        // VP Verify error message 
        expect(await registerPage.getToastMessage()).toEqual(Messages.EMAIL_IS_USED, `The error message of duplicative email is not as expected`);
    });
});