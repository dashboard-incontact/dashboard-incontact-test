import { Logger, LogTag } from "@utilities/general/logger"
import { BrowserManager } from "@utilities/protractor/browser-manager"
import { PageURL } from "@test-data/page-url"
import { ResetPasswordPage } from "@page-objects/reset-password-page"
import { LoginPage } from "@page-objects/login-page"
import { RegisterPage } from "@page-objects/register-page"
import { UserDataObject } from "@data-objects/user"
import { gmailAccounts } from "@test-data/gmail-account"
import { TestManagementPage } from "@page-objects/test-management"
import { GmailAccount } from "@utilities/api/gmail/gmail-api-type"
import { Utility } from "@utilities/general/utility"
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers"

describe('DB_LOB_RP-001', async () => {

    let gmailAccount: GmailAccount = gmailAccounts.apiTest;
    let loginPage: LoginPage;
    let registerPage: RegisterPage;
    let resetPasswordPage: ResetPasswordPage;
    let testManagementPage: TestManagementPage;
    let newAccount: UserDataObject;

    beforeEach(async () => {
        Logger.force(LogTag.TESTCASE, 'DB_LOB_RP-001 Verify that user logs in successfully with the new password after resetting password')
        jasmine.addMatchers(CompareCustomMatchers);
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.REGISTER);
        registerPage = await RegisterPage.getInstance();
        newAccount = await registerPage.registerNewRandomAccount(Utility.createAliasEmail(gmailAccount.email));
        await BrowserManager.getCurrentBrowser().restartBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
    });

    it('Verify that user logs in successfully with the new password after resetting password', async () => {
        // 1. Navigate to the "Dashboard" website
        loginPage = LoginPage.getInstance();

        // 2. Click on the "Forgot Password?" hyperlink
        await loginPage.clickOnResetPasswordLink();

        // VP The "Forgot Password" page displays.
        resetPasswordPage = ResetPasswordPage.getInstance();
        expect(await resetPasswordPage.isLoadingDisplayed()).toBeTruthy('The reset password page is not displayed');

        // 3. Enter the valid email address which is associated to the valid account (ex: "man.nguyen@logigear.com")
        await resetPasswordPage.enterEmail(newAccount.email);

        // 4. Click on the "Submit" button
        await resetPasswordPage.clickSubmitButton();

        // 5. Access to the mail inbox of the email address given in step 3, find the reset password email and get the new password.
        newAccount.password = await resetPasswordPage.checkResetPasswordEmail(newAccount.username, gmailAccount);

        // 6. Navigate to the "Login" page
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
        loginPage = LoginPage.getInstance();

        // 7. Log in with the new password specified in step 5
        await loginPage.enterLoginForm(newAccount);
        await loginPage.clickLoginButton();

        // VP. Check "Manage Tests" page displayed
        testManagementPage = TestManagementPage.getInstance();
        expect(await testManagementPage.checkPageDisplayed()).toBeTruthy('The Manage Tests page does not display');
        expect(await testManagementPage.getUserName()).toEqual(`Hi, ${newAccount.username}`, 'The username displays incorrectly on Test Management page');
    });

    afterEach(async () => {
        Logger.force(LogTag.AFTER_EACH, Logger.lineString);
        testManagementPage = TestManagementPage.getInstance();
        await testManagementPage.logout();
    });
});