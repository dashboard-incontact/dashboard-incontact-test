import { LoginPage } from "@page-objects/login-page";
import { BrowserManager } from "@utilities/protractor/browser-manager";
import { PageURL } from "@test-data/page-url";
import { Logger, LogTag } from "@utilities/general/logger";
import { userList } from "@test-data/user-data";
import { TestManagementPage } from "@page-objects/test-management";
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers";

describe('DB_LOB_LOGIN-010', async () => {

    let loginPage: LoginPage;
    let testManagementPage: TestManagementPage;
    let validUser = userList.user1;
    let newTabId: string;
    let currentTabId: string;

    beforeEach(async () => {
        Logger.force(LogTag.TESTCASE, 'DB_LOB_LOGIN-010 Verify that the "Manage Tests" page displays when navigating to the "Login" page after logging in successfully.');
        jasmine.addMatchers(CompareCustomMatchers);
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
    });

    it('Verify that the "Manage Tests" page displays when navigating to the "Login" page after logging in successfully.', async () => {

        // 1. Navigate to the "Dashboard" website
        loginPage = LoginPage.getInstance();

        // 2. Enter the valid username and password
        await loginPage.enterLoginForm(validUser);

        // 3. Click on the "Login" button
        await loginPage.clickLoginButton();

        // 4. Open the second browser tab and navigate to the "Login" page
        currentTabId = await BrowserManager.getCurrentBrowser().getCurrentTabId();
        newTabId = await BrowserManager.getCurrentBrowser().switchToNewTab();
        await BrowserManager.getCurrentBrowser().switchToTab(newTabId);
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);

        // VP The "Manage Tests" page displays again with correct user
        testManagementPage = TestManagementPage.getInstance();
        expect(await testManagementPage.checkPageDisplayed()).toBeTruthy("The Manage Tests page is not displayed");
        expect(await testManagementPage.getUserName()).toEqual(`Hi, ${validUser.username}`, 'The username displays incorrectly on Manage Tests page');
    });

    afterEach(async () => {
        Logger.force(LogTag.AFTER_EACH, Logger.lineString);

        // Log out second tab
        testManagementPage = TestManagementPage.getInstance();
        await testManagementPage.logout();

        // Log out first tab
        await BrowserManager.getCurrentBrowser().switchToTab(currentTabId);
        testManagementPage = TestManagementPage.getInstance();
        await testManagementPage.logout();
    });
});