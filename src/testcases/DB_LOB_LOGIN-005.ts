import { LoginPage } from "@page-objects/login-page";
import { BrowserManager } from "@utilities/protractor/browser-manager";
import { PageURL } from "@test-data/page-url";
import { Logger, LogTag } from "@utilities/general/logger";
import { userList } from "@test-data/user-data";
import { CompareCustomMatchers } from "@utilities/jasmine/custom-matchers";

describe('DB_LOG_LOGIN-005', async () => {

    let loginPage: LoginPage;
    let validUser = userList.user2;

    beforeEach(async () => {
        jasmine.addMatchers(CompareCustomMatchers);
        Logger.force(LogTag.TESTCASE, 'DB_LOB_LOGIN-005 Verify that browser does not remember user credentials after user login successfully without "Remember me" option');
        await BrowserManager.startBrowser();
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);
    })

    it('Verify that browser does not remember user credentials after user login successfully without "Remember me" option', async () => {
        // 1. Navigate to Login page
        loginPage = LoginPage.getInstance();

        // 2. Make sure "Remember me" check box is not selected
        expect(await loginPage.isRememberMeChecked()).toBeFalsy('The Remember Me checkbox is checked already');

        // 3. Login with valid account information (e.g. nam.nguyen/inContact4ever)
        await loginPage.enterLoginForm(validUser);
        await loginPage.clickLoginButton();

        // 4. Close browser and open browser again
        await BrowserManager.getCurrentBrowser().restartBrowser();

        // 5. Navigate to Login page
        await BrowserManager.getCurrentBrowser().navigateTo(PageURL.LOGIN);

        // 6. Notice the loaded page
        loginPage = LoginPage.getInstance();
        expect(await loginPage.checkPageDisplayed()).toBeTruthy('The Login page is not displayed');
    });
});