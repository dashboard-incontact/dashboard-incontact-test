import { Utility } from "@utilities/general/utility"
import { exceptions } from "@utilities/general/exceptions"

export class UserDataObject {

    username: string;
    password: string;
    email: string;

    constructor(username: string, password: string, email: string) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public static initUser(jsonObject: Object): UserDataObject {
        try {
            return JSON.parse(JSON.stringify(jsonObject));
        } catch (err) {
            throw new exceptions.RuntimeError(this.initUser, err.message);
        }
    }

    public static getRandomUser(usernameLength: number = 8, passwordLength: number = 8, emailLegth: number = 8): UserDataObject {
        return new UserDataObject(Utility.getRandomStr(usernameLength),
            Utility.getRandomStr(passwordLength),
            Utility.getRandomValidEmail(emailLegth));
    }
}