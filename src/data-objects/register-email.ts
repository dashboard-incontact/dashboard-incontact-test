import { GmailMessage } from "@utilities/api/gmail/gmail-api-type";
import { exceptions } from "@utilities/general/exceptions";
import { GmailFilter } from "@utilities/api/gmail/gmail-checker";
import { DashboardEmailSender } from "@test-data/dashboard-email-sender";

/**
 * Filter class for selecting correct Registration Successfully email
 */
export class RegisterEmailFilter implements GmailFilter {

    public filter(username: string, receivedMessage: GmailMessage): boolean {
        let registerEmail: RegisterEmail = new RegisterEmail(receivedMessage);
        return receivedMessage.from == DashboardEmailSender.EMAIL_ADDRESS
            && receivedMessage.subject == DashboardEmailSender.SUCCESSFUL_REGISTER_EMAIL_SUBJECT
            && registerEmail.getUsername() == username;
    }
}

/**
 * Prepresenting a Registration Successfully email
 */
export class RegisterEmail implements GmailMessage {

    id: string;
    from: string;
    subject: string;
    content: string;
    receivedTime: number;

    constructor(message: GmailMessage) {
        this.id = message.id;
        this.from = message.from;
        this.subject = message.subject;
        this.content = message.content;
        this.receivedTime = message.receivedTime;
    }

    public getUsername(): string {
        let startPos: number = this.content.search("Hi") + "Hi".length + 1;
        let endPos: number = this.content.search(",");
        if (startPos < 0 || endPos < 0) {
            throw new exceptions.RuntimeError(this.getUsername, "The successfull register email format is different");
        }
        return this.content.substring(startPos, endPos);
    }
}