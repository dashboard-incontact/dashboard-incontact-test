import { exceptions } from "@utilities/general/exceptions";
import { Utility } from "@utilities/general/utility";

export class RegisterDataObject {

    username: string;
    email: string;
    password: string;
    confirmPassword: string;

    constructor(username: string, email: string, password: string, confirmPassword: string) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public static initRegister(jsonObject: Object): RegisterDataObject {
        try {
            return JSON.parse(JSON.stringify(jsonObject));
        } catch (err) {
            throw new exceptions.RuntimeError(this.initRegister, err.message);
        }
    }

    public static getRandomRegister(email: string): RegisterDataObject {
        let username = Utility.getRandomStr(10);
        let aliasEmail = Utility.createAliasEmail(email);
        let pass = Utility.getRandomStr(10);
        return new RegisterDataObject(username, aliasEmail, pass, pass);
    }
};