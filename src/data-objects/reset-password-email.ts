import { GmailMessage } from "@utilities/api/gmail/gmail-api-type";
import { exceptions } from "@utilities/general/exceptions";
import { GmailFilter } from "@utilities/api/gmail/gmail-checker";
import { DashboardEmailSender } from "@test-data/dashboard-email-sender";

const PRE_PASSWORD_TEXT: string = "Please use the new password \\*";
const POST_PASSWORD_TEXT: string = "\\* and change it after login";

/**
 * Filter class for selecting correct reset password email
 */
export class ResetPasswordEmailFilter implements GmailFilter {

    public filter(username: string, receivedMessage: GmailMessage): boolean {
        let resetPassEmail: ResetPasswordEmail = new ResetPasswordEmail(receivedMessage);
        return receivedMessage.from == DashboardEmailSender.EMAIL_ADDRESS
            && receivedMessage.subject == DashboardEmailSender.RESET_PASSWORD_EMAIL_SUBJECT
            && resetPassEmail.getUsername() == username;
    }
}

/**
 * Prepresenting a reset password email
 */
export class ResetPasswordEmail implements GmailMessage {

    id: string;
    from: string;
    subject: string;
    content: string;
    receivedTime: number;

    constructor(message: GmailMessage) {
        this.id = message.id;
        this.from = message.from;
        this.subject = message.subject;
        this.content = message.content;
        this.receivedTime = message.receivedTime;
    }

    public getNewPassword(): string {
        let startPos: number = this.content.search(PRE_PASSWORD_TEXT) + PRE_PASSWORD_TEXT.length - 1;
        let endPos: number = this.content.search(POST_PASSWORD_TEXT);
        if (startPos < 0 || endPos < 0) {
            throw new exceptions.RuntimeError(this.getNewPassword, "The reset password email format is different");
        }
        return this.content.substring(startPos, endPos);
    }

    public getUsername(): string {
        let startPos: number = this.content.search("Hi") + "Hi".length + 1;
        let endPos: number = this.content.search(",");
        if (startPos < 0 || endPos < 0) {
            throw new exceptions.RuntimeError(this.getNewPassword, "The reset password email format is different");
        }
        return this.content.substring(startPos, endPos);
    }
}