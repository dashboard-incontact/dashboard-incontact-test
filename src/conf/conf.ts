import 'module-alias/register'
import { PathHelper } from '@utilities/general/path-helper'
import { Timeout } from '@test-data/timeout';
import path from 'path';
import { Reporter, BuildMetadata } from '@utilities/report/jasmine-reporter';
import { browser } from 'protractor';
import { ReportEmailSender } from '@utilities/report/report-email-sender';
import { gmailAccounts } from '@test-data/gmail-account'

let fs = require('fs-extra');
let HttpReporter = require('protractor-beautiful-reporter');

const REPORT_FOLDER = "report";

let reporter: Reporter;

export let config = {

    params: {
        buildId: "N/A",
        agent: "N/A"
    },

    framwork: 'jasmine',

    capabilities: {
        browserName: 'chrome',
        maxInstance: 2
    },

    onPrepare: () => {

        fs.removeSync(path.join(PathHelper.getRootPath(), REPORT_FOLDER));

        let buildMetadata: BuildMetadata = new BuildMetadata(browser.params.buildId,
            browser.params.agent,
            config.capabilities.browserName);

        reporter = new Reporter(buildMetadata);

        jasmine.getEnv().addReporter(reporter);

        jasmine.getEnv().addReporter(new HttpReporter({
            baseDirectory: REPORT_FOLDER,
            screenshotsSubfolder: 'images',
            docTitle: 'Dashboard inContact Test Report',
            gatherBrowserLogs: false,
            clientDefaults: {
                searchSettings: {
                    allselected: false,
                    passed: true,
                    failed: true,
                    pending: true,
                    withLog: true
                },
                columnSettings: {
                    displayTime: true,
                    displayBrowser: true,
                    displaySessionId: false,
                    displayOS: true,
                    inlineScreenshots: false
                }
            },
        }).getJasmine2Reporter());
    },

    onComplete: async () => {
        let emailSender: ReportEmailSender = new ReportEmailSender(gmailAccounts.sendEmail, {
            subject: "inContact Dashboard Automation Test Report",
            receivers: [
                "man.nguyen@logigear.com",
                "nam.hoai.nguyen@logigear.com",
                "tan.ta@logigear.com",
                "nhat.nguyen@logigear.com"
            ]
        });
        await emailSender.sendEmailReport(reporter);
    },

    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: Timeout.JASMINE_TIMEOUT
    },

    //directConnect: true,
    seleniumAddress: 'http://localhost:4444/wd/hub',

    specs: [
        `${PathHelper.TEST_CASES}/DB_LOB_LOGIN-001.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_LOGIN-002.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_LOGIN-003.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_LOGIN-004.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_LOGIN-005.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_LOGIN-008.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_LOGIN-009.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_LOGIN-010.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_REG-001.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_REG-002.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_REG-003.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_REG-004.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_REG-005.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_REG-006.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_RP-001.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_RP-002.js`,
        `${PathHelper.TEST_CASES}/DB_LOB_RP-004.js`
    ]
}