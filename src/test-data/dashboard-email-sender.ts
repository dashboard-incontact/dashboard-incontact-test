export enum DashboardEmailSender {
    EMAIL_ADDRESS = "testautomationincontact@gmail.com",
    RESET_PASSWORD_EMAIL_SUBJECT = "Reset your Dashboard password",
    SUCCESSFUL_REGISTER_EMAIL_SUBJECT = "Test Management Dashboard Registration Successfully"
}