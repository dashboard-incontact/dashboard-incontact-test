import { Utility } from "@utilities/general/utility";

export let registerList = {
    duplicateUsernameRegister: {
        username: "nam.nguyen",
        email: "apitest1512318" + "+" + Utility.getRandomStr(5) + "@gmail.com",
        password: "inContact4ever",
        confirmPassword: "inContact4ever"
    },
    duplicateEmailRegister: {
        username: "test" + Utility.getRandomStr(5),
        email: "nam.hoai.nguyen@logigear.com",
        password: "inContact4ever",
        confirmPassword: "inContact4ever"
    },
    mismatchPass: {
        username: "test" + Utility.getRandomStr(5),
        email: "apitest1512318" + "+" + Utility.getRandomStr(5) + "@gmail.com",
        password: "inContact4ever",
        confirmPassword: Utility.getRandomStr(5)
    },
    emptyFieldRegister: {
        username: "test" + Utility.getRandomStr(5),
        email: "apitest1512318" + "+" + Utility.getRandomStr(5) + "@gmail.com",
        password: "inContact4ever",
        confirmPassword: "inContact4ever"
    }
}