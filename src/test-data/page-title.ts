export enum PageTitle {
    LOGIN = "Login",
    REGISTER = "Get New Account",
    MANAGEMENT_TEST = "Manage Tests",
    RESET_PASSWORD = "Forgot Password?",
    LOGIN_TXT = "TEST MANAGEMENT DASHBOARD"
}