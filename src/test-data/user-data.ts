export let userList = {
    user1: {
        username: "man.nguyen",
        password: "inContact4ever",
        email: "man.nguyen@logigear.com"
    },
    user2: {
        username: "nam.nguyen",
        password: "inContact4ever",
        email: "nam.hoai.nguyen@logigear.com"
    },
    user3: {
        username: "nam.nguyen",
        password: "123456789",
        email: "nam.hoai.nguyen@logigear.com"
    },
    user4: {
        username: "  ",
        password: "  ",
        email: "nam.hoai.nguyen@logigear.com"
    },
    user5: {
        username: "123456789",
        password: "inContact4ever",
        email: "nam.hoai.nguyen@logigear.com"
    }
}