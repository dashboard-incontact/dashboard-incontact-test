export enum PageURL {
    LOGIN = "http://192.168.171.191:4200/#/login",
    FORGOT_PASSWORD = "http://192.168.171.191:4200/#/forgot-password",
    TEST_MANAGEMENT = "http://192.168.171.191:4200/#/dashboard/manage-tests",
    REGISTER = "http://192.168.171.191:4200/#/register"
}