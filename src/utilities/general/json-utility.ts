export class JsonUtil {

    private constructor() { }

    public static mapToArray<T>(jsonObject: any, getter: (e: any) => T): Array<T> {
        let arr: Array<T> = new Array();
        let i: number = 0;
        while (jsonObject[i] != undefined) {
            arr.push(getter(jsonObject[i]));
            i++;
        }
        return arr;
    }

    public static forEach(jsonObject: any, handler: (e: any) => void): void {
        let i: number = 0;
        while (jsonObject[i] != undefined) {
            handler(jsonObject[i]);
            i++;
        }
    }

    public static toJsonString(map: Map<string, any>): string {
        let jsonStr: string = "{";
        let i: number = 0;
        map.forEach((value, key, map) => {
            if (value instanceof Map) {
                jsonStr += `"${key}":"${this.toJsonString(value)}"`;
            } else {
                jsonStr += `"${key}":"${value}"`;
            }
            i++;
            if (i < map.size) {
                jsonStr += ",";
            }
        });
        return jsonStr + "}";
    }

    public static toJsonObject(map: Map<string, any>): Object {
        if (map == null || map == undefined || map.size < 1) {
            return {};
        }
        return JSON.parse(this.toJsonString(map));
    }
}