import { TimeUtil, TimeFormats } from "./time-utility";

export enum LogTag {
    TESTCASE = "TEST CASE",
    UI_ACTION = "UI",
    API = "API",
    ERROR = "ERROR",
    AFTER_EACH = "CLEAN UP"
}

export class Logger {

    private constructor() { }

    public static readonly lineString: string = "-------------------------";

    public static debug(tag: LogTag, msg: string): void {
        let time = TimeUtil.nowTimeString(TimeFormats.DATE_HOUR_MINUTE_SECOND);
        console.log(`[${time}] [${tag}] ${msg}`);
    }

    public static force(tag: LogTag, msg: string): void {
        let time = TimeUtil.nowTimeString(TimeFormats.DATE_HOUR_MINUTE_SECOND);
        console.log(`\n[${time}] [${tag}] ${msg}`);
    }
}