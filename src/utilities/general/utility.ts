import { exceptions } from "./exceptions";

let emailDomainNames = ['gmail.com', 'outlook.com', 'logigear.com', 'yahoo.com'];
export class Utility {

    private constructor() { }

    public static getRandomStr(length: number): string {
        let characters: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let randomstring = "";
        for (let i = 0; i < length; i++) {
            randomstring += characters.charAt(this.getRandomInt(0, characters.length - 1));
        }
        return randomstring;
    }

    public static getRandomElement(arr: Array<any>): any {
        if (arr.length > 0) {
            return arr[this.getRandomInt(0, arr.length - 1)];
        }
        return '';
    }

    public static getRandomEmail(length: number): string {
        let partLen: number = Math.floor((length - 2) / 3);
        let email: string = this.getRandomStr(partLen) + '@' + this.getRandomStr(partLen) + '.' + this.getRandomStr(partLen);
        if (email.length < length) {
            email = this.getRandomStr(length - email.length) + email;
        }
        return email;
    }

    public static getRandomValidEmail(usernameLength: number): string {
        let domain: string = emailDomainNames[this.getRandomInt(0, emailDomainNames.length - 1)];
        return this.getRandomStr(usernameLength) + "@" + domain;
    }

    public static getRandomInt(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public static async delay(delayTime: number): Promise<void> {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve();
            }, delayTime);
        })
    }

    public static createAliasEmail(email: string): string {
        let atPosition: number = email.indexOf('@');
        let preAt: string = email.slice(0, atPosition);
        let postAt: string = email.slice(atPosition, email.length);
        return preAt + "+" + (new Date()).getTime() + postAt;
    }

    public static async allOf(data: Array<any>, testcase: Function): Promise<void> {
        try {
            let i = 0;
            while (data[i] != undefined) {
                await testcase(data[i]);
                i++;
            }
        } catch (err) {
            throw new exceptions.RuntimeError(this.allOf, err.message);
        }
    }
}