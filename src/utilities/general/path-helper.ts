import path from 'path';
import { exceptions } from './exceptions';

export enum BuildEntry {
    CONF = 'conf',
    DATA_OBJECTS = 'data-objects',
    PAGE_OBJECTS = 'page-objects',
    TEST_CASES = 'testcases',
    TEST_DATA = 'test-data',
    UTILITIES = 'utilities',
    TEST = 'test'
}

export class PathHelper {

    static readonly buildFolderName: string = 'build';
    static rootPath: string | undefined;

    static readonly CONF: string = PathHelper.getBuildPath(BuildEntry.CONF);
    static readonly DATA_OBJECTS: string = PathHelper.getBuildPath(BuildEntry.DATA_OBJECTS);
    static readonly PAGE_OBJECTS: string = PathHelper.getBuildPath(BuildEntry.PAGE_OBJECTS);
    static readonly TEST_CASES: string = PathHelper.getBuildPath(BuildEntry.TEST_CASES);
    static readonly TEST_DATA: string = PathHelper.getBuildPath(BuildEntry.TEST_DATA);
    static readonly UTILITIES: string = PathHelper.getBuildPath(BuildEntry.UTILITIES);
    static readonly TEST: string = PathHelper.getBuildPath(BuildEntry.TEST);

    private constructor() { }

    static getBuildPath(entry: BuildEntry): string {
        let strPos: number = __dirname.search(this.buildFolderName);
        if (strPos < 0) {
            throw new exceptions.RuntimeError(this.getBuildPath, `"${this.buildFolderName}" directory is not exsiting`);
        }
        let rootFolder: string = __dirname.substring(0, strPos);
        return path.join(rootFolder, this.buildFolderName, entry).replace("\\", "/");
    }

    static getRootPath(): string {
        if (this.rootPath != undefined) {
            return this.rootPath;
        }
        let strPos: number = __dirname.search(this.buildFolderName);
        if (strPos < 0) {
            throw new exceptions.RuntimeError(this.getBuildPath, `"${this.buildFolderName}" directory is not exsiting`);
        }
        this.rootPath = __dirname.substring(0, strPos).replace("\\", "/");
        return this.rootPath;
    }
}