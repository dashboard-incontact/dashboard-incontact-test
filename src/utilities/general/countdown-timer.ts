export class CountdownTimer {

    private startTime: number = -1;
    private timeout: number = 0;

    constructor(timeout: number) {
        this.timeout = timeout;
    }

    public reset(timeout?: number) {
        if (timeout) {
            this.timeout = timeout;
        }
        this.startTime = -1;
    }

    public now(): number {
        return new Date().getTime();
    }

    public startIfNot(): void {
        if (this.startTime < 0) {
            this.startTime = this.now();
        }
    }

    public isTimeout(): boolean {
        if (this.startTime < 0 && this.timeout > 0) {
            return false;
        }
        return this.now() - this.startTime > this.timeout;
    }

    public getTimeOut(): number {
        return this.timeout;
    }
}