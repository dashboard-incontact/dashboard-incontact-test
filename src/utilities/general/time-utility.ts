import moment from "moment";

export enum TimeFormats {
    DATE_HOUR_MINUTE_SECOND = 'DD-MM-YYYY HH:mm:ss',
    HOUR_MINUTE_SECOND = 'HH:mm:ss',
    MINUTE_SECOND = "mm:ss"
}

export class TimeUtil {

    private constructor() { }

    public static nowTimestamp(utc?: boolean): number {
        if (utc === true) {
            return moment().utc().unix();
        }
        return moment().local().unix();
    }

    public static nowTimeString(format: string, utc?: boolean): string {
        if (utc === true) {
            return moment().utc().format(format);
        }
        return moment().format(format);
    }

    public static convertToTimestamp(timeString: string, format: string, utc?: boolean): number {
        if (utc === true) {
            return moment.utc(timeString, format).unix();
        }
        return moment(timeString, format).unix();
    }

    public static formatTime(timeStamp: number, format: string): string {
        return moment.unix(timeStamp).format(format);
    }

    public static formatLocalTime(timeStamp: number, format: string) {
        return moment.unix(timeStamp).local().format(format);
    }

    public static formatDuration(startTime: number, endTime: number, format: string): string {
        let milisecondDuration: number = startTime > 9999999999 ? (endTime - startTime) : (endTime - startTime) * 1000;
        return moment.utc(milisecondDuration).format(format);
    }
}