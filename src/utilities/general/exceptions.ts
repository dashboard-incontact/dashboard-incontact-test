import { Logger, LogTag } from "./logger";

export namespace exceptions {

    class BaseError extends Error {

        constructor(errorMessage?: string) {
            super(errorMessage);
            this.name = "Error";
        }
    }

    export class RuntimeError extends BaseError {

        constructor(func: Function, errorMessage: string) {
            super(errorMessage + " -> " + func.name);
            this.name = "RuntimeError";
        }
    }

    export class TimeoutError extends RuntimeError {

        timeout: number;

        constructor(func: Function, timeout: number) {
            super(func, `Timed out of ${timeout} milisecond`);
            this.name = "TimeoutError";
            this.timeout = timeout;
        }
    }
}