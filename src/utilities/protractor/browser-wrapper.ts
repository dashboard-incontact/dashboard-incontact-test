import { ProtractorBrowser, ElementFinder, Locator } from 'protractor';
import { exceptions } from '@utilities/general/exceptions';
import { Logger, LogTag } from '@utilities/general/logger';

export class BrowserWrapper {

    private browser: ProtractorBrowser;

    public static newInstance(browser: ProtractorBrowser): BrowserWrapper {
        return new BrowserWrapper(browser);
    }

    private constructor(browser: ProtractorBrowser) {
        this.browser = browser;
    }

    public async getCurrentTabId(): Promise<string> {
        try {
            return await this.browser.getWindowHandle();
        } catch (err) {
            throw new exceptions.RuntimeError(this.getCurrentTabId, err.message);
        }
    }

    public async switchToNewTab(): Promise<string> {
        try {
            await this.browser.executeScript('window.open()');
            let handles: string[] = await this.browser.getAllWindowHandles();
            let newHandle: string = handles[handles.length - 1];
            await this.browser.switchTo().window(newHandle);
            Logger.debug(LogTag.UI_ACTION, `Switch to new tab ${newHandle}`);
            return newHandle;
        } catch (err) {
            throw new exceptions.RuntimeError(this.switchToNewTab, err.message);
        }
    }

    public async switchToTab(tabId: string): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, `Switch to tab ${tabId}`);
            let currentWindow: string = await this.browser.getWindowHandle();
            if (tabId != currentWindow) {
                return await this.browser.switchTo().window(tabId);
            }
        } catch (err) {
            throw new exceptions.RuntimeError(this.switchToTab, err.message);
        }
    }

    public async navigateTo(url: string): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, `Navigate to ${url}`);
            await this.browser.waitForAngularEnabled(false);
            await this.browser.get(url);
        } catch (err) {
            throw new exceptions.RuntimeError(this.navigateTo, err.message);
        }
    }

    public async getTitleBarText(): Promise<string> {
        try {
            return await this.browser.getTitle();
        } catch (err) {
            throw new exceptions.RuntimeError(this.getTitleBarText, err.message);
        }
    }

    public async closeBrowser(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Close browser");
            await this.browser.close();
        } catch (err) {
            throw new exceptions.RuntimeError(this.closeBrowser, err.message);
        }
    }

    public async restartBrowser(): Promise<ProtractorBrowser> {
        try {
            Logger.debug(LogTag.UI_ACTION, "Restart browser");
            this.browser = await this.browser.restart();
            return this.browser;
        } catch (err) {
            throw new exceptions.RuntimeError(this.closeBrowser, err.message);
        }
    }

    public getProtractorBrowser(): ProtractorBrowser {
        return this.browser;
    }

    public findElement(locator: Locator): ElementFinder {
        try {
            return this.browser.element(locator);
        } catch (err) {
            throw new exceptions.RuntimeError(this.findElement, err.message);
        }
    }

    public async goBack(): Promise<void> {
        try {
            Logger.debug(LogTag.UI_ACTION, `Go back`);
            await this.browser.navigate().back();
        } catch (err) {
            throw new exceptions.RuntimeError(this.goBack, err.message);
        }
    }
}