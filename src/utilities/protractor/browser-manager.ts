import { ProtractorBrowser, protractor } from "protractor";
import { BrowserWrapper } from "./browser-wrapper";
import { exceptions } from "@utilities/general/exceptions";

export class BrowserManager {

    private static currentBrowser: BrowserWrapper | undefined;
    private static browserList: Array<BrowserWrapper> = new Array();

    public static getCurrentBrowser(): BrowserWrapper {
        if (this.currentBrowser != undefined) {
            return this.currentBrowser;
        } else {
            throw new exceptions.RuntimeError(this.getCurrentBrowser, "Browser is not started");
        }
    }

    public static forkNewBrowser(): number {
        let newBrowser: ProtractorBrowser;
        if (this.currentBrowser != undefined) {
            newBrowser = this.currentBrowser.getProtractorBrowser().forkNewDriverInstance();
        } else {
            newBrowser = protractor.browser;
        }
        this.browserList.push(BrowserWrapper.newInstance(newBrowser));
        return this.browserList.length - 1;
    }

    public static switchBrowser(id: number): BrowserWrapper {
        if (id < 0 || id >= this.browserList.length) {
            throw new exceptions.RuntimeError(this.switchBrowser, `Browser of id ${id} is not existing`);
        }
        this.currentBrowser = this.browserList[id];
        return this.currentBrowser;
    }

    public static async startBrowser(): Promise<void> {
        if (this.currentBrowser != undefined) {
            for (let i = 1; i < this.browserList.length; i++) {
                await this.browserList[i].closeBrowser();
            }
            this.browserList.splice(1);
            this.browserList[0] = BrowserWrapper.newInstance(await this.browserList[0].restartBrowser());
            this.currentBrowser = this.browserList[0];
        } else {
            this.browserList.push(BrowserWrapper.newInstance(protractor.browser));
            this.currentBrowser = this.browserList[0];
        }
    }
}