import { ElementFinder, protractor } from "protractor";
import { Locator } from 'protractor';
import { BrowserWrapper } from '@utilities/protractor/browser-wrapper'
import { exceptions } from "@utilities/general/exceptions";
import { CountdownTimer } from "@utilities/general/countdown-timer";
import { Timeout } from "@test-data/timeout";

export class ElementWrapper {

    private element: ElementFinder;
    private browserWrapper: BrowserWrapper;

    constructor(browserWrapper: BrowserWrapper, locator: Locator) {
        this.browserWrapper = browserWrapper;
        this.element = this.browserWrapper.findElement(locator);
    }

    public async getAttribute(attrName: string): Promise<string> {
        try {
            return await this.element.getAttribute(attrName);
        } catch (err) {
            throw new exceptions.RuntimeError(this.getAttribute, err.message);
        }
    }

    public async getText(): Promise<string> {
        try {
            return await this.element.getText();
        } catch (err) {
            throw new exceptions.RuntimeError(this.getText, err.message);
        }
    }

    public async isDisplayed(): Promise<boolean> {
        try {
            return await this.element.isDisplayed();
        } catch (err) {
            throw new exceptions.RuntimeError(this.isDisplayed, err.message);
        }
    }

    public async waitForClickable(timeout: number = Timeout.BROWSER_TIMEOUT): Promise<any> {
        try {
            return await this.browserWrapper.getProtractorBrowser()
                .wait(protractor.ExpectedConditions.elementToBeClickable(this.element), timeout);
        } catch (err) {
            throw new exceptions.RuntimeError(this.waitForClickable, err.message);
        }
    }

    public async waitForVisible(timeout: number = Timeout.BROWSER_TIMEOUT): Promise<any> {
        try {
            return await this.browserWrapper.getProtractorBrowser()
                .wait(protractor.ExpectedConditions.visibilityOf(this.element), timeout);
        } catch (err) {
            throw new exceptions.RuntimeError(this.waitForVisible, err.message);
        }
    }

    public async waitForInvisible(timeout: number = Timeout.BROWSER_TIMEOUT): Promise<any> {
        try {
            return await this.browserWrapper.getProtractorBrowser()
                .wait(protractor.ExpectedConditions.invisibilityOf(this.element), timeout);
        } catch (err) {
            throw new exceptions.RuntimeError(this.waitForInvisible, err.message);
        }
    }

    public async leftClick(timer: CountdownTimer = new CountdownTimer(Timeout.ELEMENT_TIMEOUT)): Promise<void> {
        if (timer.isTimeout()) {
            throw new exceptions.TimeoutError(this.leftClick, timer.getTimeOut());
        }
        try {
            timer.startIfNot()
            await this.element.click();
        } catch (err) {
            if (err.message.includes('Other element would receive the click') || err.message.includes('element is not attached to the page document')) {
                await this.leftClick(timer);
            } else {
                throw new exceptions.RuntimeError(ElementWrapper.prototype.leftClick, err.message);
            }
        }
    }

    public async enterKeys(value: any, timer: CountdownTimer = new CountdownTimer(Timeout.ELEMENT_TIMEOUT)): Promise<void> {
        if (timer.isTimeout()) {
            throw new exceptions.TimeoutError(this.leftClick, timer.getTimeOut());
        }
        try {
            timer.startIfNot();
            await this.element.clear();
            await this.browserWrapper.getProtractorBrowser().actions().mouseMove(this.element);
            await this.element.sendKeys(value);
            let enteredValue = await this.element.getAttribute('value');
            if (enteredValue != value) {
                await this.enterKeys(value, timer);
            }
        } catch (err) {
            throw new exceptions.TimeoutError(this.leftClick, timer.getTimeOut());
        }
    }

    public async clear(): Promise<void> {
        try {
            await this.element.clear();
        } catch (err) {
            throw new exceptions.RuntimeError(this.clear, err.message);
        }
    }

    public async isSelected(): Promise<boolean> {
        try {
            return await this.element.isSelected();
        } catch (err) {
            throw new exceptions.RuntimeError(this.isSelected, err.message);
        }
    }
}