import base64url from "base64url";
import { GmailAccount, GmailMessage } from "./gmail-api-type";
import { RequestOption, RequestBuilder } from "../request-option";
import { APIResponse, APICore } from "../api-core";
import { exceptions } from "@utilities/general/exceptions";
import { JsonUtil } from "@utilities/general/json-utility";

enum GmailApiUrl {
    TOKEN_URL = "https://oauth2.googleapis.com/token",
    LIST_MESSAGE_URL = "https://www.googleapis.com/gmail/v1/users/userId/messages",
    GET_MESSAGE_URL = "https://www.googleapis.com/gmail/v1/users/userId/messages/id",
    SEND_MESSAGE_URL = "https://www.googleapis.com/gmail/v1/users/userId/messages/send"
}

/**
 * Calling Gmail APIs
 * Using a pre-defined Refresh token to generate Access token for each api call
 */
export class GmailAPI {

    gmailAccount: GmailAccount;

    constructor(gmailAccount: GmailAccount) {
        this.gmailAccount = gmailAccount;
    }

    /**
     * Get an email by its id
     * The email id can be get by @method getlastEmailIdList
     * Bearer token is optional and used for authorization.
     * 
     * @param id the email id provided by Gmail API
     * @param bearerToken optional token for authorization
     */
    public async getEmail(id: string, bearerToken: string): Promise<GmailMessage> {
        try {
            let reqOpt: RequestOption = new RequestBuilder(GmailApiUrl.GET_MESSAGE_URL)
                .addParam("userId", this.gmailAccount.email)
                .addParam("id", id)
                .addParam("fields", "id,snippet,payload(headers,parts/parts(mimeType,body/data))")
                .build();
            let response: APIResponse = await APICore.get(reqOpt, bearerToken)
            if (response.statusCode == 200) {
                return this.parseMessageResponse(response);
            } else {
                throw new exceptions.RuntimeError(this.getEmail, `Api call is failed with status ${response.statusCode}`);
            }
        } catch (err) {
            throw new exceptions.RuntimeError(this.getEmail, err.message);
        }
    }

    /**
     * Parse the "get" API response to a GmailMessage object
     * 
     * @param response response of 
     */
    private parseMessageResponse(response: APIResponse): GmailMessage {
        try {
            let gmailMessage: GmailMessage = { id: "", from: "", subject: "", content: "", receivedTime: -1 };
            let payload = response.body.payload;
            gmailMessage.id = response.body.id;
            JsonUtil.forEach(payload.headers, (element: any) => {
                if (element.name == "Subject") {
                    gmailMessage.subject = element.value;
                }
                if (element.name == "From") {
                    gmailMessage.from = element.value;
                }
            });
            let bodyData: string = "";
            if (payload.body && payload.body.data) {
                bodyData = payload.body.data;
            } else if (payload.parts) {
                bodyData = payload.parts[0].parts[0].body.data;
            }
            gmailMessage.content = Buffer.from(bodyData, 'base64').toString('utf-8');
            return gmailMessage;
        } catch (err) {
            throw new exceptions.RuntimeError(this.parseMessageResponse, "Cannot parse the email content");
        }
    }

    /**
     * Send email defined in raw format
     * 
     * @param raw - Raw data of the email 
     * @param bearerToken - Authorization token
     */
    public async sendRawEmail(raw: string, bearerToken: string): Promise<void> {
        try {
            let encodedData: string = base64url(raw);
            let reqOpt: RequestOption = new RequestBuilder(GmailApiUrl.SEND_MESSAGE_URL)
                .addParam("userId", this.gmailAccount.email)
                .addBody("raw", encodedData)
                .build();
            let response: APIResponse = await APICore.post(reqOpt, bearerToken);
            if (response.statusCode != 200) {
                throw new Error(`Cannot send email`);
            }
        } catch (err) {
            throw new exceptions.RuntimeError(this.sendRawEmail, err.toString());
        }
    }

    /**
     * Get lastest email id list
     * 
     * @param maxResults max number of emails to get
     * @param bearerToken token for authorization
     */
    public async getlastEmailIdList(maxResults: number, bearerToken: string): Promise<string[]> {
        try {
            let reqOpt: RequestOption = new RequestBuilder(GmailApiUrl.LIST_MESSAGE_URL)
                .addParam("userId", this.gmailAccount.email)
                .addParam("fields", "messages/id")
                .addParam("maxResults", maxResults)
                .build();
            let response: APIResponse = await APICore.get(reqOpt, bearerToken);
            if (response.statusCode == 200) {
                return JsonUtil.mapToArray<string>(response.body.messages, (element: any) => {
                    return element.id;
                });
            } else {
                throw new Error(`Api call is failed with status ${response.statusCode}`);
            }
        } catch (err) {
            throw new exceptions.RuntimeError(this.getlastEmailIdList, err.toString());
        }
    }

    /**
     * Get a Access token by Refresh token
     */
    public async getAccessTokenByRefreshToken(): Promise<string> {
        try {
            let reqOpt: RequestOption = new RequestBuilder(GmailApiUrl.TOKEN_URL)
                .addBody("refresh_token", this.gmailAccount.refreshToken)
                .addBody("client_id", this.gmailAccount.clientId)
                .addBody("client_secret", this.gmailAccount.clientSecret)
                .addBody("grant_type", "refresh_token")
                .build();
            let response: APIResponse = await APICore.post(reqOpt);
            if (response.statusCode == 200) {
                return response.body.access_token;
            } else {
                throw new Error(`Api call is failed with status ${response.statusCode}`);
            }
        } catch (err) {
            throw new exceptions.RuntimeError(this.getAccessTokenByRefreshToken, err.toString());
        }
    }

    /**
     * Build raw html email
     * 
     * @param receivers - Email address of receiver
     * @param subject - The subject of the email
     * @param html - Html content of the mail
     * @returns - Raw email string
     */
    public buildRawHtmlEmail(receivers: string[], subject: string, html: string): string {
        let to: string = "";
        for (let i = 0; i < receivers.length; i++) {
            to += receivers[i];
            if (i != receivers.length - 1) {
                to += ", ";
            }
        }
        return `To: ${to}\r\nFrom: ${this.gmailAccount.email}\r\n` +
            `Subject: ${subject}\r\n` +
            `Content-Type: text/html; charset=UTF-8\r\n\r\n${html}`;
    }
}