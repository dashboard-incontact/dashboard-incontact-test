export interface GmailAccount {
    email: string
    password: string
    clientId: string
    clientSecret: string
    refreshToken: string
    permissionCode: string
}

export interface GmailMessage {
    id: string
    from: string
    subject: string
    content: string
    receivedTime: number
}