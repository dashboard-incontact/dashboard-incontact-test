import { GmailAccount, GmailMessage } from "./gmail-api-type";
import { GmailAPI } from "./gmail-api";
import { Utility } from "@utilities/general/utility";
import { Logger, LogTag } from "@utilities/general/logger";

export const CHECKING_INTERVAL = 5000;
export const ATTEMPTS_NUMBER = 5;
export const MAX_RESULTS = 10;

/**
 * Interface for Gmail message filter
 */
export interface GmailFilter {

    /**
     * Filter function used for selecting email from mailbox
     * 
     * @param username the user associated with the current Gmail account
     * @param receivedMessage Gmail message object
     */
    filter(username: string, receivedMessage: GmailMessage): boolean;
}

/**
 * Scheduling an email checking task
 */
export class GmailChecker {

    private gmailAccount: GmailAccount;
    private username: string;

    constructor(username: string, gmailAccount: GmailAccount) {
        this.gmailAccount = gmailAccount;
        this.username = username;
    }

    /**
     * Return the last emails selected by the filter
     * 
     * @param filter an object used for selecting the correct email
     */
    public async checkEmail(filter: GmailFilter): Promise<GmailMessage[]> {
        let gmailAPI: GmailAPI = new GmailAPI(this.gmailAccount);
        let token: string = await gmailAPI.getAccessTokenByRefreshToken();
        let idList: string[] = await gmailAPI.getlastEmailIdList(MAX_RESULTS, token);
        let messages: GmailMessage[] = new Array();
        for (let i = 0; i < idList.length; i++) {
            let message: GmailMessage = await gmailAPI.getEmail(idList[i], token);
            if (filter.filter(this.username, message)) {
                messages.push(message);
            }
        }
        return messages;
    }

    /**
     * Schedule an email checking task
     * 
     * @param filter an object used for selecting the correct email
     * @param interval check email after an interval of time
     * @param attempts the number of checking attempts
     */
    public async scheduleEmailCheckingTask(
        filter: GmailFilter,
        interval: number = CHECKING_INTERVAL,
        attempts: number = ATTEMPTS_NUMBER): Promise<GmailMessage[] | undefined> {

        let i = 1;
        while (i <= attempts) {
            Logger.debug(LogTag.API, `Checking email inbox (${i}/${attempts})`);
            let messages: GmailMessage[] = await this.checkEmail(filter);
            if (messages.length > 0) {
                return messages;
            }
            i++;
            await Utility.delay(interval);
        }
        return undefined;
    }
}