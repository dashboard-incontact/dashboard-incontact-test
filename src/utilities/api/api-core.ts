import { RequestOption } from './request-option';
import axios, { AxiosResponse, AxiosError } from 'axios';
import { JsonUtil } from '../general/json-utility';

/**
 * Storing response data and status code
 */
export interface APIResponse {
    statusCode: number;
    body: any;
}

/**
 * Performing HTTP request including GET, POST
 */
export class APICore {

    /**
     * Perform GET request
     * 
     * @param options contains header, param and body of the request
     * @param bearerToken baerer token for authorization
     */
    public static async get(options: RequestOption, bearerToken?: string): Promise<APIResponse> {
        if (bearerToken) {
            options.header.set("Authorization", `Bearer ${bearerToken}`);
        }
        return new Promise((resolve, reject) => {
            axios({
                method: "GET",
                url: options.url + options.getParam(),
                headers: JsonUtil.toJsonObject(options.header)
            }).then((response: AxiosResponse) => {
                let res: APIResponse = {
                    statusCode: response.status,
                    body: response.data
                };
                resolve(res);
            }).catch((error: AxiosError) => {
                reject(error);
            });
        });
    }

    /**
     * Perform POST request
     * 
     * @param options contains header, param and body of the request
     * @param bearerToken baerer token for authorization
     */
    public static async post(options: RequestOption, bearerToken?: string): Promise<APIResponse> {
        if (bearerToken) {
            options.header.set("Authorization", `Bearer ${bearerToken}`);
        }
        return new Promise((resolve, reject) => {
            axios({
                method: "POST",
                url: options.url + options.getParam(),
                headers: JsonUtil.toJsonObject(options.header),
                data: JsonUtil.toJsonObject(options.body)
            }).then((response: AxiosResponse) => {
                let res: APIResponse = {
                    statusCode: response.status,
                    body: response.data
                };
                resolve(res);
            }).catch((error: AxiosError) => {
                reject(error);
            });
        });
    }
}