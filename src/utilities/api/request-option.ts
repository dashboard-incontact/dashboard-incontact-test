import { JsonUtil } from "../general/json-utility";

/**
 * Prepresenting a HTTP request options
 * Storing params, headers and body of the request
 * Using RequestBuilder to instance new object
 */
export class RequestOption {

    public url: string;
    public param: Map<string, any>;
    public header: Map<string, any>;
    public body: Map<string, any>;

    constructor(builder: RequestBuilder) {
        this.url = builder.url;
        this.header = builder.header;
        this.body = builder.body;
        this.param = builder.param;
    }

    public getHeader(): string {
        return JsonUtil.toJsonString(this.header);
    }

    public getBody(): string {
        return JsonUtil.toJsonString(this.body);
    }

    public getParam(): string {
        if (this.param.size < 1) {
            return "";
        }
        let param: string = "?";
        let i: number = 0;
        this.param.forEach((value: any, key: string) => {
            param += `${key}=${value}`;
            i++;
            if (i < this.param.size) {
                param += "&";
            }
        });
        return param;
    }
}

/**
 * Builder class used for creating RequestOption instance
 */
export class RequestBuilder {

    public url: string;
    public param: Map<string, any>;
    public header: Map<string, any>;
    public body: Map<string, any>;

    constructor(url: string,
        headers: Map<string, any> = new Map<string, any>(),
        body: Map<string, any> = new Map<string, any>(),
        param: Map<string, any> = new Map<string, any>()
    ) {
        this.url = url;
        this.header = headers;
        this.body = body;
        this.param = param;
    }

    public addHeader(key: string, value: any): RequestBuilder {
        if (typeof (value) != "undefined") {
            this.header.set(key, value);
        }
        return this;
    }

    public addParam(key: string, value: any): RequestBuilder {
        if (typeof (value) != "undefined") {
            this.param.set(key, value);
        }
        return this;
    }

    public addBody(key: string, value: any): RequestBuilder {
        if (typeof (value) != "undefined") {
            this.body.set(key, value);
        }
        return this;
    }

    public build(): RequestOption {
        return new RequestOption(this);
    }
}