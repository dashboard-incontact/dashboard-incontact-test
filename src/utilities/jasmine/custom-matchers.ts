import MatchersUtil = jasmine.MatchersUtil;
import CustomMatcherFactories = jasmine.CustomMatcherFactories;
import CustomEqualityTester = jasmine.CustomEqualityTester;
import CustomMatcher = jasmine.CustomMatcher;
import CustomMatcherResult = jasmine.CustomMatcherResult;

export const CompareCustomMatchers: CustomMatcherFactories = {

  toEqual: (util: MatchersUtil, customEqualityTesters: readonly CustomEqualityTester[]): CustomMatcher => {
    return {
      compare: (actual: any, expect: any, expectationFailOutput?: string): CustomMatcherResult => {
        var result = { pass: false, message: '' };
        if (util.equals(actual, expect, customEqualityTesters)) {
          result.pass = true;
          result.message = "toEqual passed";
        } else {
          result.pass = false;
          result.message = `Expected ` + `'` + actual + `'` + ` to equal ` + `'` + expect + `'`;
          if (expectationFailOutput) {
            result.message += ` -> '${expectationFailOutput}'`;
          }
          throw new Error(result.message);
        }
        return result;
      }
    }
  },

  toBeTruthy: (util: MatchersUtil, customEqualityTesters: readonly CustomEqualityTester[]): CustomMatcher => {
    return {
      compare: (actual: boolean, expectationFailOutput?: string): CustomMatcherResult => {
        var result = { pass: false, message: '' };
        if (actual === true) {
          result.pass = true;
          result.message = "toBeTruthy passed";
        } else {
          result.pass = false;
          result.message = `Expected ` + `'` + actual + `'` + ` to be truthy`;
          if (expectationFailOutput) {
            result.message += ` -> '${expectationFailOutput}'`;
          }
          throw new Error(result.message);
        }
        return result;
      }
    }
  },

  toBe: (util: MatchersUtil, customEqualityTesters: readonly CustomEqualityTester[]): CustomMatcher => {
    return {
      compare: (actual: boolean, expect: boolean, message?: string): CustomMatcherResult => {
        var result = { pass: false, expectationFailOutput: '' };
        if (actual === true) {
          result.pass = true;
          result.expectationFailOutput = "toBe passed";
        } else {
          result.pass = false;
          result.expectationFailOutput = `Expected ` + `'` + actual + `'` + ` to be ${expect}`;
          if (message) {
            result.expectationFailOutput += ` -> '${message}'`;
          }
          throw new Error(result.expectationFailOutput);
        }
        return result;
      }
    }
  },
}