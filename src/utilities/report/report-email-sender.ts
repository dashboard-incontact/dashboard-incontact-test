import { Reporter } from "./jasmine-reporter";
import { HtmlReportBuilder, TemplateType } from "./email-template/html-report-builder";
import { PathHelper } from "@utilities/general/path-helper";
import { GmailAccount } from "@utilities/api/gmail/gmail-api-type";
import { GmailAPI } from "@utilities/api/gmail/gmail-api";
import { Logger, LogTag } from "@utilities/general/logger";
import { browser } from "protractor";

let fs = require("fs");

export class ReportEmailSender {

    private gmailAccount: GmailAccount;
    private receivers: string[];
    private subject: string;

    constructor(gmailAccount: GmailAccount, config: any) {
        this.gmailAccount = gmailAccount;
        this.receivers = config.receivers;
        this.subject = config.subject;
    }

    public async sendEmailReport(report: Reporter): Promise<void> {
        try {
            let rootPath: string = PathHelper.getRootPath();
            let source: string = fs.readFileSync(rootPath + "./src/assests/report-email.handlebars").toString();
            let builder: HtmlReportBuilder = new HtmlReportBuilder(source, TemplateType.HANDLEBARS);
            let html: string = builder.renderHtml(report);
            let gmailAPI: GmailAPI = new GmailAPI(this.gmailAccount);
            let rawEmail: string = gmailAPI.buildRawHtmlEmail(this.receivers, this.subject, html);
            let token: string = await gmailAPI.getAccessTokenByRefreshToken();
            await gmailAPI.sendRawEmail(rawEmail, token);
            fs.writeFileSync(rootPath + "report/email-report.html", html);
        } catch (err) {
            Logger.debug(LogTag.ERROR, err.message);
        }
    }
}