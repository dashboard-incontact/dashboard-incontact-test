import { TimeUtil, TimeFormats } from "@utilities/general/time-utility";

export enum TestCaseStatus {
    PASSED = "Passed",
    FAILED = "Failed",
    UNDEFINED = "Undefined"
}

export enum ExpectationStatus {
    MATCHER_FAILED = "Matcher failed",
    ERROR_FAILED = "Error failed",
    PASSED = "Passed",
    UNDEFINED = "Undefined"
}

export class Expectation {
    public status: ExpectationStatus = ExpectationStatus.UNDEFINED;
    public message: string = "";
    public stack: string = "";
}

export interface TestResultStatistics {
    totalTestcases: number;
    passedTestcases: number;
    failedTestcases: number;
}

export class TestCaseResult {

    public id: string = "";
    public description: string = "";
    public status: TestCaseStatus = TestCaseStatus.UNDEFINED;
    public startTime: number = 0;
    public endTime: number = 0;
    public expectations: Array<Expectation> = new Array<Expectation>();

    public addExpectation(expectation: Expectation): void {
        this.expectations.push(expectation);
    }

    public getMessage(): string {
        if (this.status == TestCaseStatus.FAILED && this.expectations.length > 0) {
            return this.expectations[0].message;
        }
        return "";
    }

    public getDurationString(): string {
        return TimeUtil.formatDuration(this.startTime, this.endTime, TimeFormats.MINUTE_SECOND);
    }
}