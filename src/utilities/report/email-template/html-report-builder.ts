import { Reporter } from "../jasmine-reporter";
import * as Handlebars from 'handlebars';
import { TestResultStatistics, TestCaseResult, TestCaseStatus } from "../test-case-result";

export enum TemplateType {
    HANDLEBARS = "handlebars"
}

export class HtmlReportBuilder {

    private source: string;
    private type: TemplateType;

    constructor(template: string, type: TemplateType) {
        this.source = template;
        this.type = type;
    }

    public renderHtml(report: Reporter): string {
        switch (this.type) {
            case TemplateType.HANDLEBARS:
                return this.renderHtmlForHandlebars(report);
        }
    }

    private renderHtmlForHandlebars(report: Reporter): string {
        let template: any = Handlebars.compile(this.source);
        let stat: TestResultStatistics = report.getTestResultStatistics();
        let testCases: Array<any> = new Array<any>();
        let i: number = 1;
        report.getTestCaseResults().forEach((testCaseResult: TestCaseResult) => {
            testCases.push({
                id: i++,
                testCaseId: testCaseResult.id,
                summary: testCaseResult.description,
                result: testCaseResult.status,
                passed: testCaseResult.status == TestCaseStatus.PASSED,
                error: testCaseResult.getMessage(),
                duration: testCaseResult.getDurationString()
            });
        });
        let reportStatus: string = report.getBuildStatus();
        let data = {
            buildId: report.getMetadata().buildId,
            startTime: report.getStartTimeString(),
            endTime: report.getEndTimeString(),
            duration: report.getTotalDurationString(),
            agent: report.getMetadata().agent,
            environment: report.getMetadata().environment,
            status: reportStatus,
            passed: reportStatus == TestCaseStatus.PASSED,
            totalTestcases: stat.totalTestcases,
            passedTestcases: stat.passedTestcases,
            failedTestcases: stat.failedTestcases,
            passRate: Math.floor((stat.passedTestcases / stat.totalTestcases) * 100) + "%",
            testCases: testCases,
            totalDuration: report.getTotalDurationString(),
        }
        return template(data);
    }
}