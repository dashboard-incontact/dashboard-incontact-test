import { TestCaseResult, TestCaseStatus, TestResultStatistics } from "./test-case-result";
import { JsonUtil } from "@utilities/general/json-utility";
import { Expectation, ExpectationStatus } from "./test-case-result";
import { TimeUtil, TimeFormats } from "@utilities/general/time-utility";
import moment = require("moment");

export interface JasmineReporter {
    jasmineStarted(suiteInfo: any): void;
    jasmineDone(suiteInfo: any): void;
    specStarted(result: any): void;
    specDone(result: any): void;
    suiteStarted(result: any): void;
    suiteDone(result: any): void;
}

export class BuildMetadata {

    public buildId: string;
    public agent: string;
    public environment: string;

    constructor(id: string, agent: string, environment: string) {
        this.buildId = id;
        this.agent = agent;
        this.environment = environment;
    }
}

export class Reporter implements JasmineReporter {

    private metadata: BuildMetadata;
    private testCaseResults: Array<TestCaseResult> = new Array<TestCaseResult>();
    private currentTestCaseResult: TestCaseResult = new TestCaseResult();
    private startTime: number = 0;
    private endTime: number = 0;

    constructor(metadata: BuildMetadata) {
        this.metadata = metadata;
    }

    public jasmineStarted(suiteInfo: any): void {
        this.startTime = TimeUtil.nowTimestamp(true);
    }

    public async jasmineDone(suiteInfo: any): Promise<void> {
        this.endTime = TimeUtil.nowTimestamp(true);
    }

    public specStarted(result: any): void {
        this.currentTestCaseResult.startTime = TimeUtil.nowTimestamp(true);
        this.currentTestCaseResult.description = result.description;
    }

    public specDone(result: any): void {
        this.currentTestCaseResult.endTime = TimeUtil.nowTimestamp(true);
        if (result.status == "passed") {
            this.currentTestCaseResult.status = TestCaseStatus.PASSED;
        } else if (result.status == "failed") {
            this.currentTestCaseResult.status = TestCaseStatus.FAILED;
        }
        this.setExpections(result.failedExpectations);
        this.setExpections(result.passedExpectations);
    }

    public suiteStarted(result: any): void {
        this.currentTestCaseResult = new TestCaseResult();
        this.currentTestCaseResult.id = result.fullName;
    }

    public suiteDone(result: any): void {
        this.testCaseResults.push(this.currentTestCaseResult);
    }

    private setExpections(failedExpectations: any): void {
        JsonUtil.forEach(failedExpectations, (e: any) => {
            let expectation: Expectation = new Expectation();
            if (e.passed == true) {
                expectation.status = ExpectationStatus.PASSED;
            } else if (e.matcherName == "") {
                expectation.status = ExpectationStatus.ERROR_FAILED;
                expectation.stack = e.stack;
            } else {
                expectation.status = ExpectationStatus.MATCHER_FAILED;
            }
            expectation.message = e.message;
            this.currentTestCaseResult.addExpectation(expectation);
        });
    }

    public getMetadata(): BuildMetadata {
        return this.metadata;
    }

    public getTestCaseResults(): Array<TestCaseResult> {
        return this.testCaseResults;
    }

    public getBuildStatus(): string {
        for (let i = 0; i < this.testCaseResults.length; i++) {
            if (this.testCaseResults[i].status == TestCaseStatus.FAILED
                || this.testCaseResults[i].status == TestCaseStatus.UNDEFINED) {
                return TestCaseStatus.FAILED;
            }
        }
        return TestCaseStatus.PASSED;
    }

    public getStartTimeString(): string {
        return TimeUtil.formatLocalTime(this.startTime, TimeFormats.DATE_HOUR_MINUTE_SECOND);
    }

    public getEndTimeString(): string {
        return TimeUtil.formatLocalTime(this.endTime, TimeFormats.DATE_HOUR_MINUTE_SECOND);
    }

    public getTestResultStatistics(): TestResultStatistics {
        let totalTcs: number = 0;
        let passedTcs: number = 0;
        let failedTcs: number = 0;
        this.testCaseResults.forEach((testResult: TestCaseResult) => {
            if (testResult.status == TestCaseStatus.FAILED) {
                failedTcs++;
            } else if (testResult.status == TestCaseStatus.PASSED) {
                passedTcs++;
            }
            totalTcs++;
        });
        let stat: TestResultStatistics = {
            totalTestcases: totalTcs,
            passedTestcases: passedTcs,
            failedTestcases: failedTcs
        };
        return stat;
    }

    public getTotalDurationString(): string {
        let totalDuration: number = 0;
        this.testCaseResults.forEach((e: TestCaseResult) => {
            totalDuration += e.endTime - e.startTime;
        });
        return TimeUtil.formatDuration(0, totalDuration, TimeFormats.MINUTE_SECOND);
    }
}